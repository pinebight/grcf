/*
 * Copyright (c) 2021-2022 Denis Remezov
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#ifndef GRCF_CRYPTO_PBKDF_H_INCLUDED
#define GRCF_CRYPTO_PBKDF_H_INCLUDED

#include <stddef.h>
#include <stdint.h>


/* Crypto support for key derivation
 *
 * This provides a bridge interface to the actual implementation
 * of crypto operations, which can be selected at build time.
 */

/* The below is not intended to guarantee any level of security, and is not a
 * substitute for a higher-level policy for checking for password quality.
 * (bytes)
 */
#define GRCF_PBKDF_PASSWD_SIZE_MIN  4

/* Currently the salt size is unique and fixed at compile time.
 * (bytes)
 */
#define GRCF_PBKDF_SALT_SIZE        32

/* As per OWASP recommendation (FIPS-140, HMAC-SHA-256; 2021). */
#define PBKDF2_SHA256_ITERATIONS    310000

/* Derive a key based on a password string
 *
 * The KDF algorithm is currently fixed at PKCS5 PBKDF2 HMAC.
 * When the operation is successful, the result is guaranteed to be the
 * same across different invocations for the same password value.
 *
 * The output key size is currently fixed at GRCF_SYM_KEY_SIZE.
 *
 * return 0 on success, STATUS_... on error.
 */
int grcf_pbkdf(uint8_t *key, size_t key_size,
               char const *passwd, size_t passwd_size,
               uint8_t const *salt, size_t salt_size);

#endif
