/* 
 * Copyright (c) 2021-2022 Denis Remezov
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#ifndef GRCF_COMMON_H_INCLUDED
#define GRCF_COMMON_H_INCLUDED

#include "grcf_ipc.h"

#include <stdlib.h>


/* Top-level command to perform */
enum grcf_cmd {
    GRCF_CMD_INVALID        = 0,
    GRCF_CMD_HELP           = 1,
    GRCF_CMD_VERSION        = 2,
    GRCF_CMD_ENC            = 3,
    GRCF_CMD_DEC            = 4,
    GRCF_CMD_ENCSM          = 5,
    GRCF_CMD_DECSM          = 6,
    GRCF_CMD_CREATE_FOB     = 7,
    GRCF_CMD_MOUNT          = 8
};

/* Mode of crypto operation */
enum grcf_mode {
    GRCF_MODE_INVALID       = 0,

    /* Authenticated encrypt/decrypt using an external key file */
    GRCF_MODE_KEY           = 1,

    /* Authenticated encrypt/decrypt using a password */
    GRCF_MODE_PASS          = 2,

    /* Multifactor (3-stage) authenticated encrypt/decrypt operation */
    GRCF_MODE_FULL          = 3
};


#define PASSWD_MAX_SIZE             256

/* Functional arguments specified for top-level operations */

/* GRCF_ARGS_F_ indicate which argument have been provided */
#define GRCF_ARGS_F_CMD             0x0001  /* Command */
#define GRCF_ARGS_F_MODE            0x0002  /* Crypto mode option, -m */
#define GRCF_ARGS_F_PPAGE_SIZE      0x0004  /* Padding page size, -p */
#define GRCF_ARGS_F_FOB_SIZE        0x0008  /* Key fob size, -s */

/* Command-specific flags */
#define GRCF_CMDF_MOUNT_RO          0x0001  /* Mount read-only */

struct grcf_auth {
    char                *path_key;      /* For GRCF_MODE_KEY and GRCF_MODE_FULL */
    size_t              passwd_len;     /* For GRCF_MODE_PASS and GRCF_MODE_FULL */
    char                passwd[PASSWD_MAX_SIZE + 2];    /* as above */
};

struct grcf_args {
    int                 argc;
    char                **argv;
    enum grcf_cmd       cmd;
    enum grcf_mode      mode;
    unsigned            flags;          /* GRCF_ARGS_F_... | */
    unsigned            op_flags;       /* GRCF_CMDF_... | */
    char                *path_in;       /* ENC, DEC, DECSM, MOUNT */
    char                *path_out;      /* ENC, DEC, MOUNT, CREATE_FOB */
    char                *path_mount;    /* mount point for MOUNT */
    struct grcf_auth    auth;
    size_t              ppage_size;     /* GRCF_CMD_ENC */
    size_t              c_fob_size;     /* GRCF_CMD_CREATE_FOB */
};

/* Parameters read on stdin, mode-dependent */

/* sem[0|1]|shm:<ipc_fn> + "\n\0" */
#define GRCF_IPC_PARAM_MAX_SIZE     (5 + GRCF_IPC_FNAME_SIZE + 1)

struct grcf_stdin_params {
    /* stdin: sem[0|1]:<sem_fn> */
    char                sem_fn[2][GRCF_IPC_FNAME_SIZE];
    /* stdin: shm:<shm_fn> */
    char                shm_fn[GRCF_IPC_FNAME_SIZE];
};

#endif
