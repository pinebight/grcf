/*
 * Copyright (c) 2021-2022 Denis Remezov
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#ifndef GRCF_IPC_H_INCLUDED
#define GRCF_IPC_H_INCLUDED

#include <stddef.h>

/* IPC (semaphore and shm) file name sizes */
/* Entropy bits for the name */
#define GRCF_IPC_NAME_ENTBITS       256
/* '/' + hex-encoded 64-char name + [.sem | .shm] + '\0' */
#define GRCF_IPC_FNAME_SIZE         (1 + GRCF_IPC_NAME_ENTBITS * 2 / 8 + 4 + 1)

/* Shared memory mapping descriptor
 * This descriptor is part of a shared object mapping, and is located at the
 * beginning.  The payload immediately follows the header.
 * Expected usage is to define a pointer to an object of this type
 * and use mmap/munmap.
 *
 * Used for serving memory mappings from grcf backend.
 */
struct grcf_shm_header {
    size_t                  size;           /* payload size */
    size_t                  size_mapped;    /* size mapped */
};

#endif
