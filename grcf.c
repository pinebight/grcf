/*
 * Copyright (c) 2021-2022 Denis Remezov
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include "common.h"
#include "control_io.h"
#include "file_crypt.h"
#include "key_fob.h"
#include "serve_mount.h"
#include "serve_shm.h"
#include "status_codes.h"
#include "util.h"

#include <errno.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>


struct grcf_version {
    unsigned char       major;
    unsigned char       minor;
    unsigned char       patch;
};

struct grcf_cmd_row {
    enum grcf_cmd       cmd;
    char const          *cmd_str;
    char const          *desc;
};

static struct grcf_cmd_row const grcf_cmd_table[] = {
    {GRCF_CMD_HELP,         "help",     "Display usage summary"},
    {GRCF_CMD_VERSION,      "version",  "Display program version"},
    {GRCF_CMD_ENC,          "enc",      "Encrypt a file"},
    {GRCF_CMD_DEC,          "dec",      "Decrypt a file"},
    {GRCF_CMD_ENCSM,        "encsm",    "Encrypt shared memory mapping"},
    {GRCF_CMD_DECSM,        "decsm",    "Decrypt into shared memory"},
    {GRCF_CMD_CREATE_FOB,   "fob",      "Create a key fob file"},
    {GRCF_CMD_MOUNT,        "mount",    "Serve a FUSE mount"}
};
static size_t const n_grcf_cmds = sizeof(grcf_cmd_table) / sizeof(*grcf_cmd_table);

struct grcf_option_row {
    char                opt;
    char const          *param;
    char const          *desc;
};

static struct grcf_option_row const grcf_option_table[] = {
    {'c', "mode",   "Crypto operation mode (key | pass | full)"},
    {'i', "path",   "Input file (plaintext or ciphertext)"},
    {'o', "path",   "Output file (ciphertext, plaintext or fob)"},
    {'k', "path",   "Key or key fob file"},
    {'m', "path",   "FUSE mount directory"},
    {'p', "size",   "Padding page size (must be a power of 2)"},
    {'r', NULL,     "Mount read-only"},
    {'s', "size",   "New key fob file size"}
};
static size_t const n_grcf_options = sizeof(grcf_option_table) / sizeof(*grcf_option_table);

struct grcf_mode_row {
    enum grcf_mode      mode;
    char const          *mode_str;
};

static struct grcf_mode_row const grcf_mode_table[] = {
    {GRCF_MODE_KEY,     "key"},
    {GRCF_MODE_PASS,    "pass"},
    {GRCF_MODE_FULL,    "full"}
};
static size_t const n_grcf_modes = sizeof(grcf_mode_table) / sizeof(*grcf_mode_table);

static struct grcf_version const current_version = {
    2, 3, 1
};

static void print_usage(void)
{
    size_t  i = 0;

    printf("Usage: grcf <command> [<options>]\n");

    printf("\nCOMMANDS\n");
    for (i = 0; i < n_grcf_cmds; ++i) {
        printf("\t%s\t%s\n", grcf_cmd_table[i].cmd_str, grcf_cmd_table[i].desc);
    }

    printf("\nOPTIONS\n");
    for (i = 0; i < n_grcf_options; ++i) {
        printf("\t-%c", grcf_option_table[i].opt);
        if (grcf_option_table[i].param != NULL) {
            printf(" <%s>", grcf_option_table[i].param);
        } else {
            printf("\t");
        }
        printf("\t%s\n", grcf_option_table[i].desc);
    }
}

static void print_version(void)
{
    printf("grcf %d.%d.%d\n",
           current_version.major, current_version.minor, current_version.patch);
}

static void init_grcf_args(struct grcf_args *pa, int argc, char **argv)
{
    memset(pa, 0, sizeof(*pa));
    pa->argc = argc;
    pa->argv = argv;
}

static void free_grcf_args(struct grcf_args *pa)
{
    free(pa->path_in);
    free(pa->path_out);
    free(pa->auth.path_key);
    free(pa->path_mount);
    grcf_explicit_bzero(&pa, sizeof(pa));
}

static enum grcf_mode parse_mode(char const *mode_str)
{
    size_t  i = 0;

    if (mode_str == NULL) return GRCF_MODE_INVALID;

    for (i = 0; i < n_grcf_modes; ++i) {
        if (strcmp(mode_str, grcf_mode_table[i].mode_str) == 0) {
            return grcf_mode_table[i].mode;
        }
    }

    return GRCF_MODE_INVALID;
}

static size_t parse_size(char const *size_str)
{
    char    *endptr = NULL;
    size_t  v = 0;
    int     shift = 0;

    if (size_str == NULL || *size_str == '\0') return (size_t)-1;

    v = strtol(size_str, &endptr, 0);
    if (v == LONG_MIN || v == LONG_MAX) return (size_t)-1;

    if (*endptr != '\0') {
        if (strcmp(endptr, "K") == 0) {
            shift = 10;
        } else if (strcmp(endptr, "M") == 0) {
            shift = 20;
        } else {
            return (size_t)-1;
        }
    }
    if (v > (SIZE_MAX >> shift)) return (size_t)-1;

    return v << shift;
}

/* XXX refactor validate_grcf_args? */
#if 0
#define CMD_PARAM_PATH_IN           0x0001
#define CMD_PARAM_PATH_OUT          0x0002
#define CMD_PARAM_PATH_MOUNT        0x0004
#define CMD_PARAM_F_MODE            0x0008
#define CMD_PARAM_F_MOUNT_RO        0x0010
#define CMD_PARAM_F_FOB_SIZE        0x0020
#define CMD_PARAM_F_PPAGE_SIZE      0x0040

struct grcf_cmd_reqs {
    enum grcf_cmd   cmd;
    unsigned        param_req;
};

static int validate_grcf_args_cmd(struct grcf_args const *pa)
{
}
#endif

/* Command-specific validation for
 * {GRCF_CMD_ENC, GRCF_CMD_DEC,
 *  GRCF_CMD_ENCSM, GRCF_CMD_DECSM,
 *  GRCF_CMD_MOUNT} */
static int validate_grcf_args_enc_dec_mount(struct grcf_args const *pa)
{
    if (pa->flags & GRCF_ARGS_F_FOB_SIZE) return STATUS_VALIDATION_SPURIOUS_FOB_SIZE;
    if ((pa->flags & GRCF_ARGS_F_MODE) == 0) return STATUS_VALIDATION_NO_MODE;

    switch (pa->mode) {
        case GRCF_MODE_KEY:
            if (pa->auth.path_key == NULL) return STATUS_VALIDATION_NO_KEY_PATH;
            break;
        case GRCF_MODE_PASS:
            if (pa->auth.path_key != NULL) return STATUS_VALIDATION_SPURIOUS_KEY_PATH;
            break;
        case GRCF_MODE_FULL:
            if (pa->auth.path_key == NULL) return STATUS_VALIDATION_NO_FOB_PATH;
            break;
        default:
            return STATUS_VALIDATION_INVALID_MODE;
    }

    if (pa->cmd == GRCF_CMD_ENC || pa->cmd == GRCF_CMD_DEC) {
        if (pa->path_in == NULL) return STATUS_VALIDATION_NO_INPUT_PATH;
        if (pa->path_out == NULL) return STATUS_VALIDATION_NO_OUTPUT_PATH;
        if (pa->path_mount != NULL) return STATUS_VALIDATION_SPURIOUS_MOUNT_PATH;
        if (pa->op_flags & GRCF_CMDF_MOUNT_RO) return STATUS_VALIDATION_SPURIOUS_MOUNT_RO;

        if (pa->auth.path_key != NULL && strcmp(pa->path_in, pa->auth.path_key) == 0) {
            return STATUS_VALIDATION_IN_KEY_PATHS_EQUAL;
        }

        if (strcmp(pa->path_in, pa->path_out) == 0) return STATUS_VALIDATION_IN_OUT_PATHS_EQUAL;
        if (pa->auth.path_key != NULL && strcmp(pa->path_out, pa->auth.path_key) == 0) {
            return STATUS_VALIDATION_OUT_KEY_PATHS_EQUAL;
        }
    } else if (pa->cmd == GRCF_CMD_ENCSM) {
        if (pa->mode != GRCF_MODE_FULL) return STATUS_VALIDATION_INVALID_MODE;
        if (pa->path_out == NULL) return STATUS_VALIDATION_NO_OUTPUT_PATH;
        if (pa->path_in != NULL) return STATUS_VALIDATION_SPURIOUS_INPUT_PATH;
        if (pa->path_mount != NULL) return STATUS_VALIDATION_SPURIOUS_MOUNT_PATH;
        if (pa->op_flags & GRCF_CMDF_MOUNT_RO) return STATUS_VALIDATION_SPURIOUS_MOUNT_RO;
        if (pa->auth.path_key != NULL && strcmp(pa->path_out, pa->auth.path_key) == 0) {
            return STATUS_VALIDATION_OUT_KEY_PATHS_EQUAL;
        }
    } else if (pa->cmd == GRCF_CMD_DECSM) {
        if (pa->mode != GRCF_MODE_FULL) return STATUS_VALIDATION_INVALID_MODE;
        if (pa->path_in == NULL) return STATUS_VALIDATION_NO_INPUT_PATH;
        if (pa->path_out != NULL) return STATUS_VALIDATION_SPURIOUS_OUTPUT_PATH;
        if (pa->path_mount != NULL) return STATUS_VALIDATION_SPURIOUS_MOUNT_PATH;
        if (pa->op_flags & GRCF_CMDF_MOUNT_RO) return STATUS_VALIDATION_SPURIOUS_MOUNT_RO;
        if (pa->flags & GRCF_ARGS_F_PPAGE_SIZE) return STATUS_VALIDATION_SPURIOUS_PPAGE_SIZE;
        if (pa->auth.path_key != NULL && strcmp(pa->path_in, pa->auth.path_key) == 0) {
            return STATUS_VALIDATION_IN_KEY_PATHS_EQUAL;
        }
    } else { /* GRCF_CMD_MOUNT */
        if (pa->path_in == NULL) return STATUS_VALIDATION_NO_INPUT_PATH;
        if (pa->path_out == NULL) return STATUS_VALIDATION_NO_OUTPUT_PATH;
        if (pa->path_mount == NULL) return STATUS_VALIDATION_NO_MOUNT_PATH;
        if (pa->auth.path_key != NULL && strcmp(pa->path_in, pa->auth.path_key) == 0) {
            return STATUS_VALIDATION_IN_KEY_PATHS_EQUAL;
        }
        /* The following is just an early convenience check (not meant to be reliable or exhaustive) */
        if (!is_dir(pa->path_mount)) return STATUS_VALIDATION_MOUNT_PATH_NOT_A_DIR;
        if (!is_valid_fname(pa->path_out)) return STATUS_VALIDATION_OUT_PATH_NOT_A_FILE;
    }

    if (pa->cmd == GRCF_CMD_ENC || pa->cmd == GRCF_CMD_ENCSM) {
        if (pa->flags & GRCF_ARGS_F_PPAGE_SIZE) {
            if (pa->ppage_size == (size_t)-1) return STATUS_VALIDATION_INVALID_PDSIZE;
            if (pa->ppage_size < GRCF_PADDING_PAGE_SIZE_MIN) return STATUS_VALIDATION_PDSIZE_LOW;
            if (pa->ppage_size > GRCF_PADDING_PAGE_SIZE_MAX) return STATUS_VALIDATION_PDSIZE_HIGH;
            if (!is_pow_2(pa->ppage_size)) return STATUS_VALIDATION_PDSZIE_NOT_POW2;
        }
    } else {
        if (pa->flags & GRCF_ARGS_F_PPAGE_SIZE) return STATUS_VALIDATION_SPURIOUS_PPAGE_SIZE;
    }

    return 0;
}

/* Command-specific validation for GRCF_CMD_CREATE_FOB */
static int validate_grcf_args_fob(struct grcf_args const *pa)
{
    if (pa->path_out == NULL) return STATUS_VALIDATION_NO_FOB_OUT_PATH;
    if ((pa->flags & GRCF_ARGS_F_FOB_SIZE) == 0) return STATUS_VALIDATION_NO_FOB_SIZE;
    if (!grcf_is_fob_size_valid(pa->c_fob_size)) return STATUS_VALIDATION_BAD_FOB_SIZE;

    if (pa->path_in != NULL) return STATUS_VALIDATION_SPURIOUS_INPUT_PATH;
    if (pa->auth.path_key != NULL) return STATUS_VALIDATION_SPURIOUS_KEY_PATH;
    if (pa->flags & GRCF_ARGS_F_MODE) return STATUS_VALIDATION_SPURIOUS_MODE;
    if (pa->flags & GRCF_ARGS_F_PPAGE_SIZE) return STATUS_VALIDATION_SPURIOUS_PPAGE_SIZE;
    if (pa->path_mount != NULL) return STATUS_VALIDATION_SPURIOUS_MOUNT_PATH;
    if (pa->op_flags & GRCF_CMDF_MOUNT_RO) return STATUS_VALIDATION_SPURIOUS_MOUNT_RO;

    return 0;
}

static int validate_grcf_args(struct grcf_args const *pa)
{
    if ((pa->flags & GRCF_ARGS_F_CMD) == 0 || pa->cmd == GRCF_CMD_INVALID) {
        if ((pa->flags & GRCF_ARGS_F_CMD) == 0) return STATUS_VALIDATION_NO_CMD;
        return STATUS_VALIDATION_BAD_CMD;
    }

    if (pa->cmd == GRCF_CMD_ENC || pa->cmd == GRCF_CMD_DEC ||
        pa->cmd == GRCF_CMD_ENCSM || pa->cmd == GRCF_CMD_DECSM ||
        pa->cmd == GRCF_CMD_MOUNT)
    {
        return validate_grcf_args_enc_dec_mount(pa);
    }

    if (pa->cmd == GRCF_CMD_CREATE_FOB) {
        return validate_grcf_args_fob(pa);
    }

    return 0;
}

static int run_operation(struct grcf_args *pa, int *ext_status)
{
    int     ret = 0;

    *ext_status = 0;

    switch (pa->cmd) {
        case GRCF_CMD_ENC:
        case GRCF_CMD_DEC:
        case GRCF_CMD_ENCSM:
        case GRCF_CMD_DECSM:
        case GRCF_CMD_CREATE_FOB:
        case GRCF_CMD_MOUNT:
            ret = grcf_setup_common();
            break;
        default:
            break;
    }

    if (ret != 0) return ret;

    switch (pa->cmd) {
        case GRCF_CMD_HELP:
            print_usage();
            break;
        case GRCF_CMD_VERSION:
            print_version();
            break;
        case GRCF_CMD_ENC:
        case GRCF_CMD_DEC:
            ret = enc_dec_file(pa);
            break;
        case GRCF_CMD_ENCSM:
            ret = grcf_shm_create_ct(pa);
            break;
        case GRCF_CMD_DECSM:
            ret = grcf_shm_serve_pt(pa);
            break;
        case GRCF_CMD_CREATE_FOB:
            ret = grcf_fob_create(pa->path_out, pa->c_fob_size);
            break;
        case GRCF_CMD_MOUNT:
            ret = grcf_serve_mount(pa, ext_status);
            break;
        default:
            ret = STATUS_INVALID_COMMAND;
            break;
    }

    return ret;
}

static enum grcf_cmd parse_cmd(char const *cmd_str)
{
    size_t  i = 0;

    for (i = 0; i < n_grcf_cmds; ++i) {
        if (strcmp(cmd_str, grcf_cmd_table[i].cmd_str) == 0) {
            return grcf_cmd_table[i].cmd;
        }
    }

    return GRCF_CMD_INVALID;
}

static int parse_args(struct grcf_args *pa, int argc, char **argv)
{
    int             ret = 0;
    int             opt = 0;

    /* The no argument case will be handled during validate */
    if (argc < 2) return 0;

    pa->cmd = parse_cmd(argv[1]);
    pa->flags |= GRCF_ARGS_F_CMD;

    /* Got a command and no options? */
    if (argc < 3) return 0;

    /* First option index */
    optind = 2;

    while (ret == 0 && (opt = getopt(argc, argv, "c:i:o:k:m:p:rs:")) != -1) {
        switch (opt) {
            case 'c':
                if (pa->flags & GRCF_ARGS_F_MODE) {
                    ret = STATUS_VALIDATION_DUPLICATE_OPT;
                } else {
                    pa->mode = parse_mode(optarg);
                    pa->flags |= GRCF_ARGS_F_MODE;
                }
                break;
            case 'i':
                if (pa->path_in != NULL) {
                    ret = STATUS_VALIDATION_DUPLICATE_OPT;
                } else {
                    pa->path_in = realpath(optarg, NULL);
                    if (pa->path_in == NULL) ret = STATUS_REALPATH_FAILED;
                }
                break;
            case 'o':
                if (pa->path_out != NULL) {
                    ret = STATUS_VALIDATION_DUPLICATE_OPT;
                } else {
                    if (pa->cmd == GRCF_CMD_MOUNT) {
                        /* The mount op requires a bare file name */
                        pa->path_out = strdup(optarg);
                        if (pa->path_out == NULL) ret = STATUS_OOM;
                    } else {
                        /* We need to resolve the directory part of the path
                         * in order to be able to compare the result against
                         * other values during validation; can't use realpath
                         * for this since the file may not yet exist */
                        ret = canonicalize_base_dir(&pa->path_out, optarg);
                    }
                }
                break;
            case 'k':
                if (pa->auth.path_key != NULL) {
                    ret = STATUS_VALIDATION_DUPLICATE_OPT;
                } else {
                    pa->auth.path_key = realpath(optarg, NULL);
                    if (pa->auth.path_key == NULL) ret = STATUS_REALPATH_FAILED;
                }
                break;
            case 'm':
                if (pa->path_mount != NULL) {
                    ret = STATUS_VALIDATION_DUPLICATE_OPT;
                } else {
                    pa->path_mount = realpath(optarg, NULL);
                    if (pa->path_mount == NULL) ret = STATUS_REALPATH_FAILED;
                }
                break;
            case 'p':
                if (pa->flags & GRCF_ARGS_F_PPAGE_SIZE) {
                    ret = STATUS_VALIDATION_DUPLICATE_OPT;
                } else {
                    pa->ppage_size = parse_size(optarg);
                    pa->flags |= GRCF_ARGS_F_PPAGE_SIZE;
                }
                break;
            case 'r':
                if (pa->op_flags & GRCF_CMDF_MOUNT_RO) {
                    ret = STATUS_VALIDATION_DUPLICATE_OPT;
                } else {
                    pa->op_flags |= GRCF_CMDF_MOUNT_RO;
                }
                break;
            case 's':
                if (pa->flags & GRCF_ARGS_F_FOB_SIZE) {
                    ret = STATUS_VALIDATION_DUPLICATE_OPT;
                } else {
                    pa->c_fob_size = parse_size(optarg);
                    pa->flags |= GRCF_ARGS_F_FOB_SIZE;
                }
                break;
            default:
                ret = STATUS_INVALID_ARG;
                break;
        }
    }

    if (optind != argc) {
        /* Unrecognized argument found */
        if (!ret) ret = STATUS_INVALID_ARG;
    }

    return ret;
}

int main(int argc, char **argv)
{
    int                 ret = 0;
    int                 ext_status = 0;
    struct grcf_args    pa;

    memset(&pa, 0, sizeof(pa));

    ret = grcf_pledge("stdio rpath wpath cpath tmppath tty", NULL);

    if (ret == 0) {
        init_grcf_args(&pa, argc, argv);
        ret = parse_args(&pa, argc, argv);
    }

    if (ret == 0) {
        ret = validate_grcf_args(&pa);
    }

    if (ret == 0) {
        if (!need_tty(&pa)) {
            ret = grcf_pledge("stdio rpath wpath cpath tmppath", NULL);
        }
    }

    if (ret == 0) {
        ret = run_operation(&pa, &ext_status);
    }

    if (ret != 0) {
        print_error(&pa, ret, ext_status);
    }

    free_grcf_args(&pa);

    return ret == 0 ? EXIT_SUCCESS : EXIT_FAILURE;
}
