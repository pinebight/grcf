/*
 * Copyright (c) 2022 Denis Remezov
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#ifndef GRCF_SERVE_SHM_H_INCLUDED
#define GRCF_SERVE_SHM_H_INCLUDED

#include "common.h"

/* Open a shared in-memory mapping with a plaintext, encrypt and save it
 * to a regular file
 *
 * There are three IPC objects shared with a front end process:
 * - Semaphore 0:
 *   Created and removed by front end.  Posted by grcf once IPC names have been
 *   obtained and sempahores open and when ready to read the password on stdin.
 * - Semaphore 1:
 *   Created and removed by front end.  Posted by grcf once the ciphertext
 *   file has been written or the operation failed.
 * - Shared memory object:
 *   Created and removed by front end.  See struct grcf_shm_header for layout.
 *
 * The names of all three IPC objects are read by grcf on stdin.
 * The password is read on stdin after posting Semaphore 0.
 *
 * pa   ciphertext and output file parameters
 *
 * Return 0 on success, STATUS_... on error
 */
int grcf_shm_create_ct(struct grcf_args *pa);

/* Decrypt ciphertext into a new shared memory mapping for accessing from a
 * front end parent process
 *
 * There are three IPC objects shared with a front end process:
 * - Semaphore 0:
 *   Created and removed by front end.  Posted by grcf once IPC names have been
 *   obtained and sempahores open and when ready to read the password on stdin.
 * - Semaphore 1:
 *   Created and removed by front end.  Posted by grcf when result is ready
 *   (success or not).
 * - Shared memory object:
 *   Created by grcf.  See struct grcf_shm_header for layout.  On success,
 *   unlinked by front end; on error, unlinked by grcf.
 *
 * The names of all three IPC objects are read by grcf on stdin.
 * The password is read on stdin after posting Semaphore 0.
 *
 * pa   ciphertext parameters
 *
 * Return 0 on success, STATUS_... on error
 */
int grcf_shm_serve_pt(struct grcf_args *pa);

#endif
