/*
 * Copyright (c) 2021-2022 Denis Remezov
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#ifndef GRCF_UTIL_H_INCLUDED
#define GRCF_UTIL_H_INCLUDED

#include "grcf_ipc.h"

#include <stddef.h>
#include <stdint.h>


/* Memory mapping information
 * Used for anonymous RW mappings in in enc_dec_file during encryption
 * and in serve_mount.
 * XXX This is too complicated, and needs refactoring.
 * XXX Possibly use grcf_shm_mapping for everything.
 * XXX Separately, possibly migrate fuse mount to an outside process */
struct grcf_mmapping {
    uint8_t                 *addr;          /* start address */
    size_t                  size;           /* actual size */
    size_t                  size_mapped;    /* size mapped */
};

/* Memory mapping for writing with output file state information
 * There are two variants:
 * - Regular file-backed mapping used for writing to file-backing storage
 * - Shared memory object-backed mapping, used for IPC
 */
struct grcf_wfile_mmapping;
typedef int (*grcf_wf_mapping_dtor)(struct grcf_wfile_mmapping *wfm, int status);

struct grcf_wfile_mmapping {
    uint8_t                 *addr;          /* mapped payload start address */
    size_t                  size;           /* payload size */

    union {
        struct {
            char const      *path;          /* destination file path */
            char            *tmp_path;      /* temporary file path */
        };
        struct grcf_shm_header *shm;        /* Shared memory mapping */
    };

    grcf_wf_mapping_dtor    dtor;           /* object destructor */
};

/* Perform common setup steps before any mutable operations */
int grcf_setup_common(void);

/* Secure zero-initialization
 * See explicit_bzero(3) on OpenBSD, FreeBSD or Linux.
 *
 * Ideally we would just call explicit_bzero() directly
 */
void grcf_explicit_bzero(void *p, size_t size);

/* OpenBSD:
 * See PLEDGE(2)
 * NOTE: disabled when USE_MLOCK is defined (non-default). See Makefile.
 * Returns STATUS_PLEDGE_FAILED on failure.
 *
 * non-OpenBSD:
 * Performs no operation and returns 0.
 */
int grcf_pledge(const char *promises, const char *execpromises);

/* Return 1 if v is a power of 2; 0 otherwise */
int is_pow_2(size_t v);

/* Host to and from little endian conversion */
void store_le32(uint8_t (*v)[sizeof(uint32_t)], uint32_t h);
void store_le64(uint8_t (*v)[sizeof(uint64_t)], uint64_t h);
uint32_t load_le32(uint8_t const (*v)[sizeof(uint32_t)]);
uint64_t load_le64(uint8_t const (*v)[sizeof(uint64_t)]);

/* If USE_MLOCK is defined:
 *    Call mlock with status translation
 *    Return 0 on success, error_status on error.
 * Else (default)
 *    Perform no ation and return 0.
 */
int grcf_mlock(const void *addr, size_t size, int error_status);

/* Create a file-backed mapping for reading
 * Optionally lock mapped pages in memory.
 * If successful, the mapping should be eventually released with munmap().
 */
int read_map_file(uint8_t **p, size_t* size, char const *path, int lock);

/* Create an anonymous mapping for read/write access
 *
 * If successful, the mapping should be eventually released with munmap().
 */
int rw_map_anon(uint8_t **p, size_t size);

/* Create a write-only mapping backed by a temporary file with support for
 * failsafe file updates
 *
 * path:    Final destination file path; must remain valid until
 *          write_map_finish()
 * size:    Size of the mapping to create
 *
 * A temporary file of specified size is created using specified path as base.
 * A mapping based on that temporary file is created.
 * The caller uses the mapping to write out the data.
 *
 * If successful, must be eventually followed by write_map_finish().
 */
int write_map_file_start(struct grcf_wfile_mmapping *wfm,
                         char const *path, size_t size);

/* Create a write-only mapping backed by a shared memory object
 *
 * fd:      Shared memory file descriptor open for writing with shm_open()
 * size:    Size of the payload
 *
 * The caller uses the mapping to write out the data.
 *
 * If successful, must be eventually followed by write_map_finish().
 */
int write_map_shm_start(struct grcf_wfile_mmapping *wfm, int fd, size_t size);

/* Clean up after write_map_file_start or write_map_shm_start
 *
 * Unmap the mapping if not NULL;
 * Set the mapping address and the temporary file path to NULL, so
 * there is no effect if write_map_finish() is called repeatedly.
 *
 * For regular file-backed mappings, also:
 * If status is 0, rename the temporary file to the destination,
 * else delete the temporary file; free the temporary file buffer.
 */
int write_map_finish(struct grcf_wfile_mmapping *wfm, int status);

/* Remap an existing anonymous read/write mapping with a new size
 * Existing mapping is assumed to be locked; if required by platform
 * implementation, the new mapping will be explicitly locked with mlock.
 * On failure, the old mapping remains valid */
int rw_remap_anon(struct grcf_mmapping *m, size_t new_mapped_size);

/* Unmap a mapping with optional prior purge of the contents
 *
 * The purge option should be used only for:
 * - Transient mappings (not backed up up by an output file);
 * - Error conditions
 */
void unmap_grcf_mapping(struct grcf_mmapping *ptm, int purge);

/* Return 1 if path stats successfully to a be a directory, 0 otherwise */
int is_dir(char const *path);

/* Return 1 if path could be a valid file name with no base directory
 * specification, 0 otherwise
 */
int is_valid_fname(char const *name);

/* Perform realpath() on the base directory part of path_in */
int canonicalize_base_dir(char **path_out, char const *path_in);

#endif
