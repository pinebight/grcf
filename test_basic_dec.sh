#!/bin/sh

. ./test_basic_common.sh

test_dec_full()
{
    local path_e="$path_i".enc_f
    local path_d="$path_i".dec_f
    local passwd="$1"

    echo "$passwd" | ./grcf dec -i "$path_e" -o "$path_d" -c full -k "$path_fob"
    if [ $? -ne 0 ]; then
        printf "Decryption failed\n"
        rc=12
        return
    fi

    cmp "$path_i" "$path_d"
    if [ $? -ne 0 ]; then
        printf "Decrypted file different to original\n"
        rc=13
        return
    fi
}

test_dec_pass()
{
    local path_e="$path_i".enc_p
    local path_d="$path_i".dec_p
    local passwd="$1"

    echo "$passwd" | ./grcf dec -i "$path_e" -o "$path_d" -c pass
    if [ $? -ne 0 ]; then
        printf "Decryption failed\n"
        rc=22
        return
    fi

    cmp "$path_i" "$path_d"
    if [ $? -ne 0 ]; then
        printf "Decrypted file different to original\n"
        rc=23
        return
    fi
}

test_dec_key()
{
    local path_e="$path_i".enc_k
    local path_d="$path_i".dec_k

    ./grcf dec -i "$path_e" -o "$path_d" -c key -k "$path_key"
    if [ $? -ne 0 ]; then
        printf "Decryption failed\n"
        rc=32
        return
    fi

    cmp "$path_i" "$path_d"
    if [ $? -ne 0 ]; then
        printf "Decrypted file different to original\n"
        rc=33
        return
    fi
}

check_key_file()
{
    path_key="$TEST_DIR"/key
    if [ ! -f "$path_key" ]; then
        printf "Key file (%s) not found \n" "$path_key"
        rc=1
    fi
}

check_fob_file()
{
    path_fob="$TEST_DIR"/fob
    if [ ! -f "$path_fob" ]; then
        printf "Fob file (%s) not found \n" "$path_fob"
        rc=2
    fi
}

check_data_file()
{
    path_i="$TEST_DIR"/data_"$size"
    if [ ! -f "$path_i" ]; then
        printf "Data file (%s) not found \n" "$path_i"
        rc=3
    fi
}

do_main()
{
    mkdir -p "$TEST_DIR"

    check_key_file
    [ "$rc" -eq 0 ] || return
    check_fob_file
    [ "$rc" -eq 0 ] || return

    for size in $*; do
        printf "Processing file size %d\n" "$size"

        check_data_file
        [ "$rc" -eq 0 ] || return

        test_dec_full "$TEST_PASSWD"
        [ "$rc" -eq 0 ] || return

        test_dec_pass "$TEST_PASSWD"
        [ "$rc" -eq 0 ] || return

        test_dec_key
        [ "$rc" -eq 0 ] || return
    done
}

do_main "$FILE_SIZES"

exit "$rc"
