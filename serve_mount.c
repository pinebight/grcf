/*
 * Copyright (c) 2022 Denis Remezov
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include "control_io.h"
#include "file_crypt.h"
#include "serve_mount.h"
#include "status_codes.h"
#include "util.h"

#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <time.h>
#include <unistd.h>

#include "platform_fuse.h"


/* XXX remove */
#define DBG_FUSE    0

#if (DBG_FUSE == 1)
#include <stdio.h>
#define DBG_PRINT(format, ...) printf(format "\n", ## __VA_ARGS__)
#else
#define DBG_PRINT(format, ...)
#endif

#define GRCF_CTX_MAPPED         0x0001
#define GRCF_CTX_INIT_SUCCESS   0x0002
#define GRCF_CTX_PG_DIRTY       0x0004

struct grcf_context {
    struct grcf_args const  *pa;
    size_t                  page_size;
    unsigned                flags;  /* GRCF_CTX_... | */
    struct grcf_mmapping    ptm;
    time_t                  atime;
    time_t                  ctime;
    time_t                  mtime;
    /* If status is non-0 during clean-up, data won't be written back even if
     * GRCF_CTX_PG_DIRTY is set.  This is arguably safer than writing back
     * possibly corrupted data */
    int                     status;
    int                     errsv;
};

static struct grcf_context *get_grcf_context(void)
{
    struct fuse_context *fctx = fuse_get_context();
    return fctx != NULL ? fctx->private_data : NULL;
}

static int is_pt_path(struct grcf_context *ctx, char const *path)
{
    return path[0] == '/' && strcmp(path + 1, ctx->pa->path_out) == 0;
}

static void *op_init(struct fuse_conn_info *conn)
{
    struct grcf_context     *ctx = get_grcf_context();

    DBG_PRINT("init()");

    if (ctx == NULL) return NULL;

    ctx->mtime = ctx->ctime = ctx->atime = time(NULL);
    ctx->flags |= GRCF_CTX_INIT_SUCCESS;

    return ctx;
}

static void op_destroy(void *p)
{
    struct grcf_context     *ctx = p;
    struct grcf_args const  *pa = NULL;
    int                     ret = 0;

    DBG_PRINT("destroy()");

    if (ctx == NULL) return;

    pa = ctx->pa;

    if ((ctx->flags & GRCF_CTX_MAPPED) == 0) return;

    if (ctx->flags & GRCF_CTX_PG_DIRTY && ctx->status == 0) {
        ret = enc_3stage(pa->path_in,           /* write back to original */
                         &pa->auth,
                         pa->flags & GRCF_ARGS_F_PPAGE_SIZE ?
                            pa->ppage_size : (size_t)-1,
                         ctx->ptm.addr,
                         ctx->ptm.size);
        if (ret != 0) {
            ctx->status = ret;
            ctx->errsv = errno;
        } else {
            ctx->flags &= ~GRCF_CTX_PG_DIRTY;
        }
    }
    unmap_grcf_mapping(&ctx->ptm, 1);   /* purge & unmap plaintext */
    ctx->flags &= ~GRCF_CTX_MAPPED;
}

static int op_getattr(char const *path, struct stat *st)
{
    struct grcf_context     *ctx = get_grcf_context();

    DBG_PRINT("getattr(%s)", path);

    memset(st, 0, sizeof(*st));

    if (ctx == NULL) return -ENOENT;
    if ((ctx->flags & GRCF_CTX_MAPPED) == 0) return -EIO;

    if (strcmp(path, "/") == 0) {
        st->st_mode = S_IFDIR | S_IRUSR | S_IXUSR;
        st->st_nlink = 2;
    } else if (is_pt_path(ctx, path)) {
        st->st_mode = S_IFREG | S_IRUSR;
        if ((ctx->pa->op_flags & GRCF_CMDF_MOUNT_RO) == 0) {
            st->st_mode |= S_IWUSR;
        }
        st->st_nlink = 1;
        st->st_size = ctx->ptm.size;
    } else {
        return -ENOENT;
    }

    st->st_atim.tv_sec = ctx->atime;
    st->st_mtim.tv_sec = ctx->mtime;
    st->st_ctim.tv_sec = ctx->ctime;
    st->st_uid = getuid();
    st->st_gid = getgid();

    return 0;
}

static int op_readdir(char const *path, void *buffer, fuse_fill_dir_t filler,
                      off_t ofs, struct fuse_file_info *fi)
{
    struct grcf_context     *ctx = get_grcf_context();

    if (ctx == NULL) return -ENOENT;
    if ((ctx->flags & GRCF_CTX_MAPPED) == 0) return -EIO;

    filler(buffer, ".", NULL, 0);
    filler(buffer, "..", NULL, 0);
    filler(buffer, ctx->pa->path_out, NULL, 0);

    return 0;
}

static int op_open(char const *path, struct fuse_file_info *fi)
{
    struct grcf_context     *ctx = get_grcf_context();

    DBG_PRINT("op_open(%s, flags=0x%x)", path, fi->flags);

    if (ctx == NULL) return -ENOENT;
    if ((ctx->flags & GRCF_CTX_MAPPED) == 0) return -EIO;
    if (!is_pt_path(ctx, path)) return -ENOENT;

    if ((ctx->pa->op_flags & GRCF_CMDF_MOUNT_RO) != 0 &&
        (fi->flags & O_ACCMODE) != O_RDONLY)
    {
        return -EROFS;
    }

    return 0;
}

static int op_read(char const *path, char *buffer,
                   size_t size, off_t ofs, struct fuse_file_info *fi)
{
    size_t                  f_size = 0;
    struct grcf_context     *ctx = get_grcf_context();

    if (ctx == NULL) return -ENOENT;
    if ((ctx->flags & GRCF_CTX_MAPPED) == 0) return -EIO;
    if (!is_pt_path(ctx, path)) return -ENOENT;

    f_size = ctx->ptm.size;

    if (ofs >= f_size) return 0;
    if (ofs + size > f_size) {
        size = f_size - ofs;
    }
    DBG_PRINT("read size: %zu", size);

    memcpy(buffer, ctx->ptm.addr + ofs, size);
    ctx->atime = time(NULL);

    return size;
}

static size_t page_align(struct grcf_context const *ctx, size_t size)
{
    size_t  r = size % ctx->page_size;
    if (r != 0) {
        size += ctx->page_size - r;
    }
    DBG_PRINT("aligned size for remap: %zu", size);

    return size;
}

static int op_truncate(char const *path, off_t size)
{
    struct grcf_context     *ctx = get_grcf_context();
    int                     ret = 0;

    DBG_PRINT("op_truncate(%s): %zu", path, (size_t)size);

    if (ctx == NULL) return -ENOENT;
    if ((ctx->flags & GRCF_CTX_MAPPED) == 0) return -EIO;
    if (!is_pt_path(ctx, path)) return -ENOENT;
    if (ctx->pa->op_flags & GRCF_CMDF_MOUNT_RO) return -EROFS;

    if (size > ctx->ptm.size_mapped) {
        ret = rw_remap_anon(&ctx->ptm, page_align(ctx, size));
        if (ret != 0) {
            ctx->status = ret;
            ctx->errsv = errno;
            return -ENOSPC;
        }
    }

    ctx->atime = time(NULL);
    if (size != ctx->ptm.size) {
        ctx->ptm.size = size;
        ctx->flags |= GRCF_CTX_PG_DIRTY;
        ctx->mtime = ctx->ctime = ctx->atime;
    }

    /* Reset status even if there was a previous error (assume recovery) */
    ctx->status = 0;
    return 0;
}

static size_t compute_new_size(struct grcf_context const *ctx, size_t size_min)
{
    /* Exponential growth with a base of 1.5 */
    size_t  size = ctx->ptm.size_mapped + ctx->ptm.size_mapped / 2;
    return page_align(ctx, size > size_min ? size : size_min);
}

static int op_write(char const *path, char const *buffer,
                    size_t size, off_t ofs, struct fuse_file_info *fi)
{
    struct grcf_context     *ctx = get_grcf_context();
    int                     ret = 0;
    size_t                  ofs_cur_end = 0;

    DBG_PRINT("op_write(%s): %zu", path, (size_t)size);

    if (ctx == NULL) return -ENOENT;
    if ((ctx->flags & GRCF_CTX_MAPPED) == 0) return -EIO;
    if (!is_pt_path(ctx, path)) return -ENOENT;
    if (ctx->pa->op_flags & GRCF_CMDF_MOUNT_RO) return -EROFS;

    if (size == 0) return 0;
    if (ofs < 0) return -EINVAL;

    ofs_cur_end = ofs + size;
    if (ofs_cur_end < size || ofs_cur_end < ofs) return -EINVAL;

    if (ofs_cur_end >= ctx->ptm.size_mapped) {
        DBG_PRINT("remapping with size %zu", compute_new_size(ctx, ofs_cur_end));

        ret = rw_remap_anon(&ctx->ptm, compute_new_size(ctx, ofs_cur_end));
        if (ret != 0) {
            ctx->status = ret;
            ctx->errsv = errno;
            return -ENOSPC;
        }
    }

    memcpy(ctx->ptm.addr + ofs, buffer, size);
    if (ctx->ptm.size < ofs_cur_end) {
        ctx->ptm.size = ofs_cur_end;
    }
    ctx->flags |= GRCF_CTX_PG_DIRTY;
    ctx->mtime = ctx->ctime = ctx->atime = time(NULL);

    /* Reset status even if there was a previous error (assume recovery) */
    ctx->status = 0;
    return size;
}

static struct fuse_operations f_ops = {
    .init       = op_init,
    .destroy    = op_destroy,
    .getattr    = op_getattr,
    .readdir    = op_readdir,
    .open       = op_open,
    .truncate   = op_truncate,
    .read       = op_read,
    .write      = op_write,
};

int grcf_serve_mount(struct grcf_args *pa, int *ext_status)
{
    int                 ret = 0;
    struct grcf_context  ctx;
    char                fuse_flags[2][3] = {
        "-f",
        "-s"
    };
    char                *argv[] = {
        pa->argv[0],
        fuse_flags[0],
        fuse_flags[1],
        pa->path_mount
    };
    int argc = sizeof(argv) / sizeof(*argv);

    memset(&ctx, 0, sizeof(ctx));

    ctx.page_size = sysconf(_SC_PAGESIZE);
    if (ctx.page_size == (size_t)-1) return STATUS_FAILED_TO_GET_PAGE_SIZE;

    ret = read_passwd(pa);
    if (ret != 0) return ret;

    ret = dec_3stage(pa->path_in,
                     NULL,                                      /* path_out */
                     &pa->auth,
                     (pa->op_flags & GRCF_CMDF_MOUNT_RO) == 0,  /* overmap if RW */
                     &ctx.ptm,                                  /* remember the mappping */
                     -1);
    if (ret != 0) return ret;

    ctx.pa = pa;
    ctx.flags = GRCF_CTX_MAPPED;

    ret = fuse_main(argc, argv, &f_ops, &ctx);
    if (ret != 0) {
        *ext_status = ret;
        ret = STATUS_FUSE_MAIN_FAILED;
    } else if ((ctx.flags & GRCF_CTX_INIT_SUCCESS) == 0) {
        ret = STATUS_FUSE_OP_INIT_FAILED;
    } else if (ctx.flags & GRCF_CTX_PG_DIRTY) {
        ret = ctx.status;
        if (ret == 0) {
            ret = STATUS_UNEXPECTED_FUSE_EXIT;
        }
    } else {
        ret = ctx.status;
    }

    if (ret != 0) {
        errno = ctx.errsv;
    }

    return ret;
}
