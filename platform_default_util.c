/*
 * Copyright (c) 2021-2022 Denis Remezov
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include "crypto_util.h"
#include "status_codes.h"
#include "util.h"

#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <unistd.h>


/* Secure zero memory */
#if !defined(__APPLE__)
void grcf_explicit_bzero(void *p, size_t size)
{
    explicit_bzero(p, size);
}
#endif  /* !__APPLE__ */

/* Most BSDs */
#if !defined(__APPLE__) && !defined(__gnu_linux__)
int grcf_random(uint8_t *dst, size_t dst_size)
{
    arc4random_buf(dst, dst_size);
    return 0;
}
#endif  /* !__APPLE__ && !__gnu_linux__ */

int grcf_random_u64(uint64_t *dst)
{
    return grcf_random((uint8_t*)dst, sizeof(*dst));
}

int grcf_pledge(const char *promises, const char *execpromises)
{
#if defined(GRCF_USE_PLEDGE)
    return pledge(promises, execpromises) == 0 ? 0 : STATUS_PLEDGE_FAILED;
#else
    return 0;
#endif
}

/* XXX should go away once FUSE mount is factored out. */
int rw_remap_anon(struct grcf_mmapping *m, size_t new_mapped_size)
{
    int             ret = 0;
    void            *addr = MAP_FAILED;
    size_t          new_size = m->size < new_mapped_size ? m->size : new_mapped_size;

    if (new_mapped_size == 0) {
        addr = NULL;
    } else {
        addr = mmap(NULL, new_mapped_size, PROT_READ | PROT_WRITE,
                    MAP_PRIVATE | MAP_ANON, -1, 0);
        if (addr == MAP_FAILED) return STATUS_MMAP_WR_FAILED;

        ret = grcf_mlock(addr, new_mapped_size, STATUS_MLOCK_WR_FAILED);
        if (ret != 0) {
            munmap(addr, new_mapped_size);
            return STATUS_MLOCK_WR_FAILED;
        }
    }

    if (m->addr != NULL) {
        memcpy(addr, m->addr, new_size);
        grcf_explicit_bzero(m->addr, m->size_mapped);
        munmap(m->addr, m->size_mapped);
    }

    m->addr = addr;
    m->size = new_size;
    m->size_mapped = new_mapped_size;

    return 0;
}
