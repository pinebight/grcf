/*
 * Copyright (c) 2021-2022 Denis Remezov
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include "crypto_util.h"
#include "status_codes.h"

#include <fcntl.h>
#include <unistd.h>


/* RNG generation using /dev/urandom for platforms that lack arc4random */

static char const   DEV_RANDOM[] = "/dev/urandom";

int grcf_random(uint8_t *dst, size_t dst_size)
{
    int         ret = 0;
    int         fd = -1;
    ssize_t     bytes_read = 0;

    fd = open(DEV_RANDOM, O_RDONLY);
    if (fd < 0) return STATUS_DEV_RANDOM_OPEN_FAILED;

    while (dst_size > 0) {
        bytes_read = read(fd, dst, dst_size);
        if (bytes_read < 0) {
            ret = STATUS_DEV_RANDOM_READ_FAILED;
            break;
        }
        dst += bytes_read;
        dst_size -= bytes_read;
    }

    close(fd);
    return ret;
}
