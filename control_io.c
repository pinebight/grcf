/*
 * Copyright (c) 2022 Denis Remezov
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include "control_io.h"
#include "status_codes.h"
#include "util.h"

#include <ctype.h>
#include <errno.h>
#include <signal.h>
#include <string.h>
#include <stdio.h>
#include <sys/types.h>
#include <termios.h>
#include <unistd.h>


struct grcf_diagmsg_row {
    int                 status;
    char const          *desc;
};

static struct grcf_diagmsg_row const grcf_diagmsg_table[] = {
    {STATUS_VALIDATION_NO_CMD,          "No command specified"},
    {STATUS_VALIDATION_BAD_CMD,         "Invalid command"},
    {STATUS_VALIDATION_NO_INPUT_PATH,   "Input path must be specified"},
    {STATUS_VALIDATION_NO_OUTPUT_PATH,  "Output path must be specified"},
    {STATUS_VALIDATION_SPURIOUS_FOB_SIZE,   "Fob size not needed for the command"},
    {STATUS_VALIDATION_SPURIOUS_INPUT_PATH, "Input path not needed for the command"},
    {STATUS_VALIDATION_SPURIOUS_OUTPUT_PATH, "Output path not needed for the command"},
    {STATUS_VALIDATION_SPURIOUS_MODE,       "Mode not needed for the command"},
    {STATUS_VALIDATION_SPURIOUS_PPAGE_SIZE, "Padding size not needed for the command"},
    {STATUS_VALIDATION_NO_MODE,         "No mode specified"},
    {STATUS_VALIDATION_NO_KEY_PATH,     "Key path must be specified"},
    {STATUS_VALIDATION_NO_MOUNT_PATH,   "FUSE mount must be specified"},
    {STATUS_VALIDATION_SPURIOUS_KEY_PATH,   "Key path provided in password mode"},
    {STATUS_VALIDATION_SPURIOUS_MOUNT_PATH, "Mount path not needed for the command"},
    {STATUS_VALIDATION_SPURIOUS_MOUNT_RO,   "Mount mode not needed for the command"},
    {STATUS_VALIDATION_NO_FOB_PATH,     "Key fob path must be specified"},
    {STATUS_VALIDATION_INVALID_MODE,    "Invalid mode"},
    {STATUS_VALIDATION_INVALID_PDSIZE,  "Invalid padding size"},
    {STATUS_VALIDATION_PDSIZE_LOW,      "Padding size too small"},
    {STATUS_VALIDATION_PDSIZE_HIGH,     "Padding size too high"},
    {STATUS_VALIDATION_PDSZIE_NOT_POW2, "Padding size must be a power of 2"},
    {STATUS_VALIDATION_NO_FOB_OUT_PATH, "Output path for key fob must be specified"},
    {STATUS_VALIDATION_NO_FOB_SIZE,     "Key fob file size must be specified"},
    {STATUS_VALIDATION_BAD_FOB_SIZE,    "Invalid key fob file size"},
    {STATUS_VALIDATION_IN_OUT_PATHS_EQUAL,  "Input and output paths must be different"},
    {STATUS_VALIDATION_IN_KEY_PATHS_EQUAL,  "Input and key paths must be different"},
    {STATUS_VALIDATION_OUT_KEY_PATHS_EQUAL, "Output and key paths must be different"},
    {STATUS_VALIDATION_OUT_PATH_NOT_A_FILE, "Invalid output file name"},
    {STATUS_VALIDATION_MOUNT_PATH_NOT_A_DIR, "Mount path not a directory"},
    {STATUS_VALIDATION_DUPLICATE_OPT,   "Duplicate options provided"},

    {STATUS_INVALID_ARG,                "Invalid arguments"},
    {STATUS_PASSWORD_INPUT_ERROR,       "Password entry error"},
    {STATUS_PASSWORD_EMPTY,             "Empty password entered"},
    {STATUS_PASSWORD_TOO_LONG,          "Password too long"},
    {STATUS_PASSWORD_REENTRY_MISMATCH,  "Entered passwords did not match"},
    {STATUS_PASSWORD_DISALLOWED_CHAR,   "Password contains disallowed character"},
    {STATUS_OOM,                        "Out of memory"},
    {STATUS_TCGETATTR_FAILED,           "Failed to get terminal attributes"},
    {STATUS_TCSETATTR_FAILED,           "Failed to set terminal attributes"},
    {STATUS_REALPATH_FAILED,            "Failed to get absolute path"},
    {STATUS_PATH_INVALID,               "Path invalid"},
    {STATUS_GETCWD_FAILED,              "Failed to get current working directory"},
    {STATUS_DEC_AUTH_FAILED,            "Ciphertext authentication failure"},
    {STATUS_INVALID_PASSWORD_SIZE,      "Invalid Password"},
    {STATUS_PLEDGE_FAILED,              "Pledge"}
};
static size_t const n_diagmsgs = sizeof(grcf_diagmsg_table) / sizeof(*grcf_diagmsg_table);

/* IPC parameter prefixes expected on stdin */
static char IPC_PARAM_SEM0[] = "sem0:";
static char IPC_PARAM_SEM1[] = "sem1:";
static char IPC_PARAM_SHM[] = "shm:";

static int read_passwd_once(char *passwd, size_t *size)
{
    int         ret = 0;
    size_t      len = 0;
    size_t      i = 0;

    if (fgets(passwd, *size, stdin) == NULL) {
        ret = STATUS_PASSWORD_INPUT_ERROR;
        goto out;
    }

    len = strlen(passwd);
    if (len > 0 && passwd[len - 1] == '\n') {
        --len;
        passwd[len] = '\0';
    }
    if (len == 0) {
        ret = STATUS_PASSWORD_EMPTY;
        goto out;
    }
    if (len > PASSWD_MAX_SIZE) {
        ret = STATUS_PASSWORD_TOO_LONG;
        goto out;
    }

    /* Disallow unprintable and non-ASCII characters.
     * This isn't ideal.  In particular, it would be great to be able to use
     * different alphabets, but allowing that increases the potential for a
     * mismatch due to user error, unicode denormalization and even the inability
     * to enter a password due to the lack of input device configuration.
     * It's just more robust to stick to printable ASCII for now. */
    for (i = 0; i < len; ++i) {
        if (!isascii(passwd[i]) || !isprint(passwd[i])) {
            ret = STATUS_PASSWORD_DISALLOWED_CHAR;
            goto out;
        }
    }

    *size = len;

out:
    if (ret != 0) {
        grcf_explicit_bzero(passwd, *size);
    }

    return ret;
}

static sig_atomic_t volatile    signum_caught = 0;

static void tio_sa_handler(int signo)
{
    if (signum_caught == 0) {
        signum_caught = signo;
    }
}

#define N_SIGNUMS_TO_CATCH_MAX   8

static int const signums_to_catch[N_SIGNUMS_TO_CATCH_MAX] = {
    SIGHUP,
    SIGINT,
    SIGPIPE,
    SIGQUIT,
    SIGTSTP,
    SIGTERM,
    SIGTTIN,
    SIGTTOU
};
static size_t const n_signums_to_catch = sizeof(signums_to_catch) / sizeof(*signums_to_catch);

static void set_sigations(struct sigaction *sa, struct sigaction *sa_old)
{
    unsigned        i = 0;

    /* Note that we don't restart system calls, nor do we attempt to restart
     * password entry when continuing after a stop condition.
     * As a result, password entry will fail after a stop (unless the stop
     * occurred after the 2nd entry had already been completed) - this is
     * considered to be a good thing for simplicity and security.
     */
    memset(sa, 0, sizeof(*sa));
    sa->sa_handler = tio_sa_handler;

    for (i = 0; i < n_signums_to_catch; ++i) {
        sigaction(signums_to_catch[i], sa, sa_old + i);
    }
}

static void restore_sigactions(struct sigaction const *sa_old)
{
    unsigned        i = 0;

    for (i = 0; i < n_signums_to_catch; ++i) {
        sigaction(signums_to_catch[i], sa_old + i, NULL);
    }

    if (signum_caught != 0) {
        raise(signum_caught);
        signum_caught = 0;
    }
}

static int prompt_for_passwd(struct grcf_args *pa)
{
    int                 ret = 0;
    int                 errsv = 0;
    unsigned            i = 0;
    unsigned            n_prompts = 1;
    struct termios      tios;
    tcflag_t            c_lflag_old = 0;
    char                passwd_v2[sizeof(pa->auth.passwd)];
    size_t              passwd_v2_len = 0;
    char                *passwd = pa->auth.passwd;
    size_t              *passwd_len = &pa->auth.passwd_len;
    struct sigaction    sa;
    struct sigaction    sa_old[N_SIGNUMS_TO_CATCH_MAX];

    if (pa->cmd == GRCF_CMD_ENC) {
        n_prompts = 2;
    }

    memset(&tios, 0, sizeof(tios));

    ret = tcgetattr(fileno(stdin), &tios);
    if (ret != 0) return STATUS_TCGETATTR_FAILED;

    set_sigations(&sa, sa_old);

    c_lflag_old = tios.c_lflag;
    tios.c_lflag &= ~(ECHO | ECHONL);
    ret = tcsetattr(fileno(stdin), TCSAFLUSH, &tios);
    if (ret != 0) {
        ret = STATUS_TCSETATTR_FAILED;
        goto out_restore_sa;
    }

    for (i = 0; i < n_prompts; ++i) {
        if (i == 0) {
            printf("Enter password: ");
        } else {
            passwd = passwd_v2;
            passwd_len = &passwd_v2_len;
            printf("Re-enter password to verify: ");
        }
        fflush(stdout);

        *passwd_len = sizeof(pa->auth.passwd);
        ret = read_passwd_once(passwd, passwd_len);
        printf("\n");
        if (ret != 0) goto out;
    }

    if (n_prompts > 1 && memcmp(pa->auth.passwd, passwd_v2, passwd_v2_len) != 0) {
        ret = STATUS_PASSWORD_REENTRY_MISMATCH;
    }

out:
    errsv = errno;
    grcf_explicit_bzero(passwd_v2, sizeof(passwd_v2));
    if (ret != 0) {
        grcf_explicit_bzero(pa->auth.passwd, sizeof(pa->auth.passwd));
    }

    tios.c_lflag = c_lflag_old;
    tcsetattr(fileno(stdin), TCSAFLUSH, &tios);
    errno = errsv;

out_restore_sa:
    errsv = errno;
    restore_sigactions(sa_old);
    errno = errsv;

    return ret;
}

static int need_passwd(struct grcf_args const *pa)
{
    return (pa->cmd == GRCF_CMD_ENC ||
            pa->cmd == GRCF_CMD_DEC ||
            pa->cmd == GRCF_CMD_ENCSM ||
            pa->cmd == GRCF_CMD_DECSM ||
            pa->cmd == GRCF_CMD_MOUNT) &&
           (pa->mode == GRCF_MODE_PASS ||
            pa->mode == GRCF_MODE_FULL);
}

int need_tty(struct grcf_args const *pa)
{
    /* See read_passwd() */
    return need_passwd(pa) &&
           pa->cmd != GRCF_CMD_ENCSM &&
           pa->cmd != GRCF_CMD_DECSM &&
           isatty(fileno(stdin));
}

int read_passwd(struct grcf_args *pa)
{
    int         ret = 0;

    if (!need_passwd(pa)) return STATUS_NO_PASSWD_NEEDED;

    /* Assume non-interactive use if either:
     * - We are not reading from a terminal (this is used for test scripts)
     * - Command is GGRCF_CMD_ENCSM or RCF_CMD_DECSM (grcf is executed by a
     *   front-end) */
    if (isatty(fileno(stdin)) &&
        pa->cmd != GRCF_CMD_ENCSM &&
        pa->cmd != GRCF_CMD_DECSM)
    {
        ret = prompt_for_passwd(pa);
    } else if (errno != EBADF) {
        pa->auth.passwd_len = sizeof(pa->auth.passwd);
        ret = read_passwd_once(pa->auth.passwd, &pa->auth.passwd_len);
    } else {
        ret = STATUS_BAD_STDIN;
    }

    return ret;
}

static int validate_cpy_ipc_fname(char *dst, size_t dst_size, char const *src)
{
    if (strlen(src) != GRCF_IPC_FNAME_SIZE - 1 || src[0] != '/') {
        return STATUS_IPC_PARAM_BAD_VALUE;
    }
    if (dst_size != GRCF_IPC_FNAME_SIZE) return STATUS_CE_INVALID_BUFFER_SIZE;

    strcpy(dst, src);

    return 0;
}

int read_ipc_names(struct grcf_stdin_params *prm)
{
    int         ret = 0;
    size_t      len = 0;
    unsigned    i = 0;
    char        str[GRCF_IPC_PARAM_MAX_SIZE];

    memset(prm->sem_fn, 0, sizeof(prm->sem_fn));
    memset(prm->shm_fn, 0, sizeof(prm->shm_fn));

    for (i = 0; i < 3; ++i) {
        if (fgets(str, sizeof(str), stdin) == NULL) {
            return STATUS_IPC_PARAM_INPUT_ERROR;
        }

        len = strlen(str);
        if (len > 0 && str[len - 1] == '\n') {
            --len;
            str[len] = '\0';
        } else {
            return STATUS_IPC_PARAM_BAD_VALUE;
        }

        if (strncmp(str, IPC_PARAM_SEM0, sizeof(IPC_PARAM_SEM0) - 1) == 0) {
            if (prm->sem_fn[0][0] != '\0') return STATUS_IPC_PARAM_DUP_VALUE;
            ret = validate_cpy_ipc_fname(prm->sem_fn[0],
                                         GRCF_IPC_FNAME_SIZE,
                                         str + sizeof(IPC_PARAM_SEM0) - 1);
            if (ret != 0) return ret;
        } else if (strncmp(str, IPC_PARAM_SEM1, sizeof(IPC_PARAM_SEM1) - 1) == 0) {
            if (prm->sem_fn[1][0] != '\0') return STATUS_IPC_PARAM_DUP_VALUE;
            ret = validate_cpy_ipc_fname(prm->sem_fn[1],
                                         GRCF_IPC_FNAME_SIZE,
                                         str + sizeof(IPC_PARAM_SEM1) - 1);
            if (ret != 0) return ret;
        } else if (strncmp(str, IPC_PARAM_SHM, sizeof(IPC_PARAM_SHM) - 1) == 0) {
            if (prm->shm_fn[0] != '\0') return STATUS_IPC_PARAM_DUP_VALUE;
            ret = validate_cpy_ipc_fname(prm->shm_fn,
                                         GRCF_IPC_FNAME_SIZE,
                                         str + sizeof(IPC_PARAM_SHM) - 1);
            if (ret != 0) return ret;
        } else {
            return STATUS_IPC_PARAM_BAD_VALUE;
        }
    }

    return 0;
}

void print_error(struct grcf_args const *pa, int status, int ext_status)
{
    size_t      i = 0;
    int         errsv = errno;

    for (i = 0; i < n_diagmsgs; ++i) {
        if (status == grcf_diagmsg_table[i].status) {
            fprintf(stderr, "ERROR: %s", grcf_diagmsg_table[i].desc);
            break;
        }
    }

    if (i == n_diagmsgs) {
        fprintf(stderr, "ERROR: Operation failed: 0x%08x", status);
    }
    if (STATUS_HAS_EXT(status)) {
        fprintf(stderr, ": extended status: 0x%x", ext_status);
    }
    if (STATUS_HAS_ERRNO(status)) {
        fprintf(stderr, ": %s", strerror(errsv));
    }
    fprintf(stderr, "\n");

    if (status == STATUS_VALIDATION_NO_CMD || status == STATUS_VALIDATION_BAD_CMD) {
        fprintf(stderr, "See \"%s help\" for help.\n", pa->argv[0]);
    }
}
