/*
 * Copyright (c) 2022 Denis Remezov
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include "control_io.h"
#include "file_crypt.h"
#include "serve_shm.h"
#include "status_codes.h"
#include "util.h"

#include <errno.h>
#include <fcntl.h>
#include <semaphore.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <string.h>
#include <unistd.h>


#define GRCF_SHM_F_CTX_VALID     0x0001     /* context object valid */

struct grcf_shm {
    unsigned                    flags;      /* GRCF_SHM_F_... | */
    struct grcf_stdin_params    param;
    sem_t                       *sem[2];
};

static int grcf_shm_init(struct grcf_shm *ctx)
{
    int                 ret = 0;
    unsigned            i = 0;

    ctx->sem[0] = ctx->sem[1] = SEM_FAILED;
    ctx->flags = GRCF_SHM_F_CTX_VALID;

    for (i = 0; i < 2; ++i) {
        ctx->sem[i] = sem_open(ctx->param.sem_fn[i], 0);
        if (ctx->sem[i] == SEM_FAILED) return STATUS_SEM_OPEN_FAILED;
    }

    ret = sem_post(ctx->sem[0]);
    if (ret != 0) return STATUS_SEM_POST_FAILED;

    return 0;
}

static void grcf_shm_free(struct grcf_shm *ctx)
{
    unsigned        i = 0;

    if ((ctx->flags & GRCF_SHM_F_CTX_VALID) == 0) return;

    for (i = 0; i < 2; ++i) {
        if (ctx->sem[i] != SEM_FAILED) {
            sem_close(ctx->sem[i]);
        }
    }

    memset(ctx, 0, sizeof(*ctx));
}

int grcf_shm_create_ct(struct grcf_args *pa)
{
    int                     ret = 0, ret1 = 0;
    int                     errsv = 0;
    struct grcf_shm         ctx;
    int                     fd = -1;
    struct stat             st;
    struct grcf_shm_header  *shm = MAP_FAILED;

    memset(&ctx, 0, sizeof(ctx));
    memset(&st, 0, sizeof(st));

    ret = read_ipc_names(&ctx.param);
    if (ret != 0) return ret;

    ret = grcf_shm_init(&ctx);
    if (ret != 0) return ret;

    ret = read_passwd(pa);
    if (ret != 0) goto out;

    fd = shm_open(ctx.param.shm_fn, O_RDWR, 0);
    if (fd < 0) {
        ret = STATUS_SHM_OPEN_FAILED;
        goto out;
    }

    /* Size, validate, map the shared memory object and validate the format */
    ret = fstat(fd, &st);
    if (ret != 0) {
        ret = STATUS_STAT_FAILED;
        goto out;
    }
    if (st.st_size < sizeof(struct grcf_shm_header)) {
        ret = STATUS_IPC_BAD_SHM_SIZE;
        goto out;
    }

    shm = mmap(NULL, st.st_size, PROT_READ, MAP_SHARED, fd, 0);
    if (shm == MAP_FAILED) {
        ret = STATUS_MMAP_RD_FAILED;
        goto out;
    }

    if (shm->size_mapped != st.st_size) {
        ret = STATUS_IPC_BAD_SHM_MAPPED_SIZE;
        goto out;
    }
    if (shm->size > SIZE_MAX - sizeof(struct grcf_shm_header) ||
        shm->size + sizeof(struct grcf_shm_header) > shm->size_mapped)
    {
        ret = STATUS_IPC_BAD_SHM_PAYLOAD_SIZE;
        goto out;
    }

    /* Finally write out the ciphertext file */
    ret = enc_3stage(pa->path_out,
                     &pa->auth,
                     pa->flags & GRCF_ARGS_F_PPAGE_SIZE ? pa->ppage_size : (size_t)-1,
                     (uint8_t const *)(shm + 1),
                     shm->size);

    ret1 = sem_post(ctx.sem[1]);
    if (ret1 != 0 && ret == 0) {
        ret = STATUS_SEM_POST_FAILED;
    }

out:
    errsv = errno;

    if (shm != MAP_FAILED) {
        munmap(shm, st.st_size);
    }
    if (fd >= 0) {
        close(fd);
    }
    grcf_shm_free(&ctx);

    errno = errsv;

    return ret;
}

int grcf_shm_serve_pt(struct grcf_args *pa)
{
    int                     ret = 0, ret1 = 0;
    int                     errsv = 0;
    struct grcf_shm         ctx;
    int                     fd = -1;

    memset(&ctx, 0, sizeof(ctx));

    ret = read_ipc_names(&ctx.param);
    if (ret != 0) return ret;

    ret = grcf_shm_init(&ctx);
    if (ret != 0) return ret;

    ret = read_passwd(pa);
    if (ret != 0) goto out;

    fd = shm_open(ctx.param.shm_fn, O_CREAT | O_EXCL | O_RDWR, S_IRUSR | S_IWUSR);
    if (fd < 0) {
        ret = STATUS_SHM_OPEN_FAILED;
        goto out;
    }

    ret = dec_3stage(pa->path_in,
                     NULL,      /* path_out */
                     &pa->auth,
                     0,         /* no overmap; XXX implement for RW ? */
                     NULL,      /* grcf_mmapping */
                     fd);

    ret1 = sem_post(ctx.sem[1]);
    if (ret1 != 0 && ret == 0) {
        ret = STATUS_SEM_POST_FAILED;
    }

out:
    errsv = errno;

    if (fd >= 0) {
        close(fd);
        if (ret != 0) {
            /* The front-end is responsible for deleting it on success */
            shm_unlink(ctx.param.shm_fn);
        }
    }
    grcf_shm_free(&ctx);

    errno = errsv;

    return ret;
}
