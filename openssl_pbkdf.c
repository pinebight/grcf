/*
 * Copyright (c) 2021-2022 Denis Remezov
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include "crypto_pbkdf.h"
#include "crypto_symciph.h"
#include "status_codes.h"

#include <limits.h>
#include <openssl/evp.h>
#include <openssl/hmac.h>


int grcf_pbkdf(uint8_t *key, size_t key_size,
               char const *passwd, size_t passwd_size,
               uint8_t const *salt, size_t salt_size)
{
    int             ret = 0;
    EVP_MD const    *digest = EVP_sha256();

    if (passwd_size < GRCF_PBKDF_PASSWD_SIZE_MIN) return STATUS_INVALID_PASSWORD_SIZE;
    if (passwd_size > INT_MAX) return STATUS_SIZE_OUT_OF_RANGE;
    if (salt_size != GRCF_PBKDF_SALT_SIZE) return STATUS_INVALID_SALT_SIZE;
    if (key_size != GRCF_SYM_KEY_SIZE) return STATUS_INVALID_KEY_SIZE;

    ret = PKCS5_PBKDF2_HMAC(passwd, passwd_size,
                            salt, salt_size,
                            PBKDF2_SHA256_ITERATIONS, digest,
                            key_size, key);
    if (ret != 1) return STATUS_PBKDF2_HMAC_FAILED;

    return 0;
}
