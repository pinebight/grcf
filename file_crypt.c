/*
 * Copyright (c) 2021-2022 Denis Remezov
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include "control_io.h"
#include "crypto_pbkdf.h"
#include "crypto_symciph.h"
#include "crypto_util.h"
#include "file_crypt.h"
#include "key_fob.h"
#include "status_codes.h"
#include "util.h"

#include <errno.h>
#include <string.h>
#include <sys/mman.h>
#include <unistd.h>


/* Stage 1/2 header
 * All integer fields are in Little Endian */
struct grcf_ct_header {
    /* Currently expected to be GRCF_3ST_HEADER_VERSION
     * Could be used in the future to switch between layouts if
     * a new version is introduced */
    uint8_t         version[sizeof(uint32_t)];
    /* Currently GRCF_PF_1 */
    uint8_t         profile[sizeof(uint32_t)];
    /* Stage 1 payload size (IV | ciphertext | tag) */
    uint8_t         payload_size[sizeof(uint64_t)];
    /* A SHA-256 digest of the fob plaintext associated with this ciphertext;
     * verified during decryption as a sanity check */
    uint8_t         fob_pt_digest[GRCF_FOB_DGST_SIZE];
    /* Key area size for the associated fob */
    uint8_t         key_area_size[sizeof(uint64_t)];
    /* Key offset within fob data for stage 1 */
    uint8_t         key_offset[sizeof(uint64_t)];
    /* Key offset within fob pad data for stage 2 */
    uint8_t         pad_offset[sizeof(uint64_t)];
    uint8_t         reserved[64];   /* reserved for future profiles */
};

/* Host-endian in-memory copy of integer fields of grcf_ct_header */
struct grcf_ct_header_host {
    uint32_t        version;
    uint32_t        profile;
    uint64_t        payload_size;
    uint64_t        key_area_size;
    uint64_t        key_offset;
    uint64_t        pad_offset;
};

/* Store header from host endian to little endian */
static void store_header(struct grcf_ct_header *hdr_le,
                         struct grcf_ct_header_host const *hdr_h)
{
    store_le32(&hdr_le->version, hdr_h->version);
    store_le32(&hdr_le->profile, hdr_h->profile);
    store_le64(&hdr_le->payload_size, hdr_h->payload_size);
    store_le64(&hdr_le->key_area_size, hdr_h->key_area_size);
    store_le64(&hdr_le->key_offset, hdr_h->key_offset);
    store_le64(&hdr_le->pad_offset, hdr_h->pad_offset);
}

/* Load header from little endian into host endian */
static void load_header(struct grcf_ct_header_host *hdr_h,
                        struct grcf_ct_header const *hdr_le)
{
    hdr_h->version = load_le32(&hdr_le->version);
    hdr_h->profile = load_le32(&hdr_le->profile);
    hdr_h->payload_size = load_le64(&hdr_le->payload_size);
    hdr_h->key_area_size = load_le64(&hdr_le->key_area_size);
    hdr_h->key_offset = load_le64(&hdr_le->key_offset);
    hdr_h->pad_offset = load_le64(&hdr_le->pad_offset);
}

/* Get concatenated IV/Ciphertext/Tag size for a given plaintext size
 * Return (size_t)-1 if the value provided would result in an overflow.
 */
static size_t to_ict_size(size_t pt_size)
{
    size_t const    delta = GRCF_AEAD_IV_SIZE + GRCF_AUTHTAG_SIZE;
    if (pt_size > SIZE_MAX - delta) return (size_t)-1;
    return pt_size + delta;
}

/* Get concatenated Salt/IV/Ciphertext/Tag size for a given plaintext size
 * Return (size_t)-1 if the value provided would result in an overflow.
 */
static size_t to_sict_size(size_t pt_size)
{
    size_t const    delta = GRCF_PBKDF_SALT_SIZE + GRCF_AEAD_IV_SIZE + GRCF_AUTHTAG_SIZE;
    if (pt_size > SIZE_MAX - delta) return (size_t)-1;
    return pt_size + delta;
}

/* Get plaintext size for a given IV/Ciphertext/Tag size
 * Return (size_t)-1 if the value provided is out of bounds.
 */
static size_t from_ict_size(size_t ict_size)
{
    size_t const    delta = GRCF_AEAD_IV_SIZE + GRCF_AUTHTAG_SIZE;
    if (ict_size < delta) return (size_t)-1;
    return ict_size - delta;
}

/* Get plaintext size for a given Salt/IV/Ciphertext/Tag size
 * Return (size_t)-1 if the value provided is out of bounds.
 */
static size_t from_sict_size(size_t sict_size)
{
    size_t const    delta = GRCF_PBKDF_SALT_SIZE + GRCF_AEAD_IV_SIZE + GRCF_AUTHTAG_SIZE;
    if (sict_size < delta) return (size_t)-1;
    return sict_size - delta;
}

/* Perform authenticated encryption, generating and saving a random nonce
 * On success, the output has the IV/Ciphertext/Tag format.
 */
static int enc_file(uint8_t *dst, size_t dst_size,
                    uint8_t const *src, size_t src_size,
                    uint8_t const *key, size_t key_size)
{
    int         ret = 0;
    uint8_t     *iv = dst;
    size_t      ict_size = to_ict_size(src_size);

    if (ict_size == (size_t)-1 || dst_size != ict_size) {
        return STATUS_CE_INVALID_BUFFER_SIZE;
    }

    ret = grcf_random(iv, GRCF_AEAD_IV_SIZE);
    if (ret != 0) return ret;

    return grcf_encrypt_auth(dst + GRCF_AEAD_IV_SIZE, dst_size - GRCF_AEAD_IV_SIZE,
                             src, src_size,
                             key, key_size,
                             iv, GRCF_AEAD_IV_SIZE);
}

/* XOR a pepper into a key; both must be GRCF_SYM_KEY_SIZE in size,
 * which is currently fixed at 32.
 */
static inline int xor_64(uint8_t const *key, size_t key_size,
                         uint8_t const *pepper, size_t pepper_size)
{
    if (key_size != 32) return STATUS_CE_INVALID_KEY_SIZE;
    if (pepper_size != 32) return STATUS_CE_INVALID_PEPPER_SIZE;

    ((uint64_t*)key)[0] ^= ((uint64_t*)pepper)[0];
    ((uint64_t*)key)[1] ^= ((uint64_t*)pepper)[1];
    ((uint64_t*)key)[2] ^= ((uint64_t*)pepper)[2];
    ((uint64_t*)key)[3] ^= ((uint64_t*)pepper)[3];

    return 0;
}

/* Perform authenticated encryption, generating and saving a random nonce
 * and a random salt for password-based key derivation.
 * If pepper is non-NULL, XOR it into the derived key before encryption.
 * On success, the output has the Salt/IV/Ciphertext/Tag format.
 */
static int enc_file_pass(uint8_t *dst, size_t dst_size,
                         uint8_t const *src, size_t src_size,
                         char const *passwd, size_t passwd_len,
                         uint8_t const *pepper, size_t pepper_size)
{
    int         ret = 0;
    uint8_t     key_pass[GRCF_SYM_KEY_SIZE];
    uint8_t     *salt = dst;
    size_t      sict_size = to_sict_size(src_size);

    if (sict_size == (size_t)-1 || dst_size != sict_size) {
        return STATUS_CE_INVALID_BUFFER_SIZE;
    }

    ret = grcf_random(salt, GRCF_PBKDF_SALT_SIZE);
    if (ret != 0) return ret;

    ret = grcf_pbkdf(key_pass, sizeof(key_pass),
                     passwd, passwd_len,
                     salt, GRCF_PBKDF_SALT_SIZE);
    if (ret != 0) return ret;

    if (pepper != NULL) {
        ret = xor_64(key_pass, sizeof(key_pass), pepper, pepper_size);
        if (ret != 0) goto out;
    }

    ret = enc_file(dst + GRCF_PBKDF_SALT_SIZE, dst_size - GRCF_PBKDF_SALT_SIZE,
                   src, src_size,
                   key_pass, sizeof(key_pass));
out:
    grcf_explicit_bzero(key_pass, sizeof(key_pass));
    return ret;
}

/* Perform authenticated decryption, assuming IV/Ciphertext/Tag layout */
static int dec_file(uint8_t *dst, size_t dst_size,
                    uint8_t const *src, size_t src_size,
                    uint8_t const *key, size_t key_size)
{
    uint8_t const   *iv = src;
    size_t          req_dst_size = from_ict_size(src_size);

    if (req_dst_size == (size_t)-1 || req_dst_size != dst_size) {
        return STATUS_CE_INVALID_BUFFER_SIZE;
    }

    return grcf_decrypt_auth(dst, dst_size,
                             src + GRCF_AEAD_IV_SIZE, src_size - GRCF_AEAD_IV_SIZE,
                             key, key_size,
                             iv, GRCF_AEAD_IV_SIZE);
}

/* Perform authenticated decryption, assuming Salt/IV/Ciphertext/Tag layout
 * If pepper is non-NULL, XOR it into the derived key before decryption.
 */
static int dec_file_pass(uint8_t *dst, size_t dst_size,
                         uint8_t const *src, size_t src_size,
                         char const *passwd, size_t passwd_len,
                         uint8_t const *pepper, size_t pepper_size)
{
    int             ret = 0;
    uint8_t         key_pass[GRCF_SYM_KEY_SIZE];
    uint8_t const   *salt = src;
    uint8_t const   *iv = NULL;
    size_t const    ct_offset = GRCF_PBKDF_SALT_SIZE + GRCF_AEAD_IV_SIZE;
    size_t          req_dst_size = from_sict_size(src_size);

    if (req_dst_size == (size_t)-1 || req_dst_size != dst_size) {
        return STATUS_CE_INVALID_BUFFER_SIZE;
    }

    ret = grcf_pbkdf(key_pass, sizeof(key_pass),
                     passwd, passwd_len,
                     salt, GRCF_PBKDF_SALT_SIZE);
    if (ret != 0) return ret;

    if (pepper != NULL) {
        ret = xor_64(key_pass, sizeof(key_pass), pepper, pepper_size);
        if (ret != 0) goto out;
    }

    iv = src + GRCF_PBKDF_SALT_SIZE;
    ret = grcf_decrypt_auth(dst, dst_size,
                            src + ct_offset, src_size - ct_offset,
                            key_pass, GRCF_SYM_KEY_SIZE,
                            iv, GRCF_AEAD_IV_SIZE);
out:
    grcf_explicit_bzero(key_pass, sizeof(key_pass));
    return ret;
}

/* Head function for 1-stage authenticated encryption/decryption */
static int enc_dec_file_1stage(char const *path_in,
                               char const *path_out,
                               enum grcf_cmd cmd,
                               enum grcf_mode mode,
                               struct grcf_auth const *auth)
{
    int                         ret = 0;
    int                         errsv = 0;

    uint8_t                     *key = NULL;
    size_t                      key_size = 0;
    uint8_t                     *src = NULL;
    size_t                      src_size = 0;
    size_t                      dst_size = 0;
    struct grcf_wfile_mmapping  dstm;

    memset(&dstm, 0, sizeof(dstm));

    ret = read_map_file(&src, &src_size, path_in, 0);
    if (ret != 0) goto out;

    if (cmd == GRCF_CMD_ENC) {
        dst_size = mode == GRCF_MODE_KEY ? to_ict_size(src_size) :
                                           to_sict_size(src_size);
        if (dst_size == (size_t)-1) {
            ret = STATUS_DATA_SIZE_OVERFLOW;
            goto out;
        }
    } else {
        dst_size = mode == GRCF_MODE_KEY ? from_ict_size(src_size) :
                                           from_sict_size(src_size);
        if (dst_size == (size_t)-1) {
            ret = STATUS_DECRYPT_BAD_SRC_SIZE;
            goto out;
        }
    }

    ret = write_map_file_start(&dstm, path_out, dst_size);
    if (ret != 0) goto out;

    if (mode == GRCF_MODE_KEY) {
        ret = read_map_file(&key, &key_size, auth->path_key, 1);
        if (ret != 0) goto out;

        if (key_size != GRCF_SYM_KEY_SIZE) {
            ret = STATUS_INVALID_KEY_FILE_SIZE;
            goto out;
        }

        ret = cmd == GRCF_CMD_ENC ?
            enc_file(dstm.addr, dst_size, src, src_size, key, key_size) :
            dec_file(dstm.addr, dst_size, src, src_size, key, key_size);
    } else {
        ret = cmd == GRCF_CMD_ENC ?
            enc_file_pass(dstm.addr, dst_size, src, src_size, auth->passwd, auth->passwd_len, NULL, 0) :
            dec_file_pass(dstm.addr, dst_size, src, src_size, auth->passwd, auth->passwd_len, NULL, 0);
    }
    if (ret != 0) goto out;

    ret = write_map_finish(&dstm, 0);

out:
    errsv = errno;
    write_map_finish(&dstm, ret);
    if (src != NULL) {
        munmap(src, src_size);
    }
    if (key != NULL) {
        munmap(key, key_size);
    }
    errno = errsv;

    return ret;
}

/* Compute padding page size based on source file size
 * The current algorithm is:
 *  - Compute nlp2: next-larger-or-equeal-power-of-2 of source size
 *  - Set ppage size to nlp2 / 16
 *  - Ensure ppage size is within predefined min/max range
 *
 * This is policy-level logic (and could be easily changed).
 */
static size_t compute_ppage_size(size_t src_size)
{
    /* Residual size */
    size_t      res_size = src_size;
    /* Next-larger (or equal) power of 2 of src_size */
    size_t      nlp2 = 1;
    size_t      ppage_size = 0;

    while (res_size != 0 && src_size != nlp2) {
        res_size &= ~nlp2;
        nlp2 <<= 1;
    }

    ppage_size = nlp2 >> 4;

    if (ppage_size < GRCF_PADDING_PAGE_SIZE_MIN) {
        ppage_size = GRCF_PADDING_PAGE_SIZE_MIN;
    } else if (ppage_size > GRCF_PADDING_PAGE_SIZE_MAX) {
        ppage_size = GRCF_PADDING_PAGE_SIZE_MAX;
    }

    return ppage_size;
}

/* Choose the output (stage 3) size using a hybrid padding scheme:
 *  1. Round up to align at ppage_size;
 *  2. Add a random offset [0, ppage_size).
 *
 * Here, (1) reduces information leaks due to output size, while (2) mainly
 * obfuscates the fact that the output is a result of a size padding scheme.
 *
 * Note: ppage_size must be a power of 2.
 */
static int enc_compute_padded_size(size_t *padded_size, size_t raw_size, size_t ppage_size)
{
    int         ret = 0;
    uint64_t    r = 0;
    size_t      rup_size = (raw_size + ppage_size - 1) & ~(ppage_size - 1);
    size_t      rdown_size = raw_size & ~(ppage_size - 1);
    size_t      delta = ppage_size + (rdown_size == raw_size ? 0 : ppage_size);

    /* Ensure padding won't cause an overflow */
    if (rdown_size > SIZE_MAX - delta) return STATUS_DATA_SIZE_OVERFLOW;

    ret = grcf_random_u64(&r);
    if (ret != 0) return ret;

    *padded_size = rup_size + (r % ppage_size);
    return 0;
}

/* Compute intermediate and final sizes for 3-stage encryption */
static int enc_compute_sizes(size_t *st3_size, size_t *st1_size,
                             size_t *payload_size, size_t *padding_size,
                             size_t src_size, size_t ppage_size)
{
    int     ret = 0;
    size_t  st1_payload_size = to_ict_size(src_size);
    /* Sizes exclusive of padding */
    size_t  raw_st1_size = sizeof(struct grcf_ct_header) + st1_payload_size;
    size_t  raw_st3_size = to_sict_size(raw_st1_size);

    if (st1_payload_size == (size_t)-1 ||
        st1_payload_size > SIZE_MAX - sizeof(struct grcf_ct_header) ||
        raw_st3_size == (size_t)-1)
    {
        return STATUS_DATA_SIZE_OVERFLOW;
    }

    if (ppage_size == (size_t)-1) {
        ppage_size = compute_ppage_size(src_size);
    } else {
        if (ppage_size < GRCF_PADDING_PAGE_SIZE_MIN ||
            ppage_size > GRCF_PADDING_PAGE_SIZE_MAX ||
            !is_pow_2(ppage_size))
        {
            return STATUS_INVALID_PPAGE_SIZE;
        }
    }

    /* Add padding to arrive at the final output size */
    ret = enc_compute_padded_size(st3_size, raw_st3_size, ppage_size);
    if (ret != 0) return ret;

    *padding_size = *st3_size - raw_st3_size;
    *payload_size = st1_payload_size;
    *st1_size = raw_st1_size + *padding_size;
    return 0;
}

/* XOR stage 1 payload with a key stream from a fob pad */
static void xor_st1_payload(struct grcf_key_fob const *fob, uint8_t *payload,
                            size_t payload_size)
{
    size_t              i = 0;
    size_t              n_dws = payload_size / sizeof(uint64_t);
    uint8_t const       *pad = fob->pad_area;
    size_t              pad_off = fob->pad_offset;

    for (i = 0; i < n_dws; ++i) {
        ((uint64_t*)payload)[i] ^= *(uint64_t*)(pad + pad_off);
        pad_off += sizeof(uint64_t);
        pad_off %= fob->pad_area_size;
    }

    /* Last 1-7 bytes outside of 8-byte alignment (if any) */
    for (i *= sizeof(uint64_t); i < payload_size; ++i) {
        /* pad_off guaranteed within pad_area_size due to pad_area_size alignment */
        payload[i] ^= pad[pad_off];
        ++pad_off;
    }
}

/* Process stage 1 data in place for encryption by XOR-ing payload and
 * initializing the padding.
 */
static int enc_perform_st2(struct grcf_key_fob const *fob, uint8_t *payload,
                           size_t payload_size, size_t padding_size)
{
    xor_st1_payload(fob, payload, payload_size);
    return grcf_random(payload + payload_size, padding_size);
}

int enc_3stage(char const *path_out,
               struct grcf_auth const *auth,
               size_t ppage_size,
               uint8_t const *pt_addr,
               size_t pt_size)
{
    int                         ret = 0;
    int                         errsv = 0;
    struct grcf_key_fob         fob;

    uint8_t                     *stage1 = NULL;
    size_t                      stage1_size = 0;
    struct grcf_ct_header       *hdr_le = NULL;
    struct grcf_ct_header_host  hdr_host;
    uint8_t                     *payload = NULL;
    size_t                      payload_size = 0;
    size_t                      padding_size = 0;
    size_t                      dst_size = 0;
    struct grcf_wfile_mmapping  dstm;

    memset(&fob, 0, sizeof(fob));
    memset(&hdr_host, 0, sizeof(hdr_host));
    memset(&dstm, 0, sizeof(dstm));

    ret = enc_compute_sizes(&dst_size, &stage1_size,
                            &payload_size, &padding_size,
                            pt_size, ppage_size);
    if (ret != 0) goto out;

    /* Stage 1: src (i.e. plaintext) -> stage1 */
    ret = rw_map_anon(&stage1, stage1_size);
    if (ret != 0) goto out;

    hdr_le = (struct grcf_ct_header*)stage1;
    payload = stage1 + sizeof(struct grcf_ct_header);

    ret = grcf_fob_map(&fob, auth->path_key, auth->passwd, auth->passwd_len);
    if (ret != 0) goto out;

    ret = grcf_fob_init_new(&fob, hdr_le->fob_pt_digest, sizeof(hdr_le->fob_pt_digest));
    if (ret != 0) goto out;

    hdr_host.version = GRCF_3ST_HEADER_VERSION;
    hdr_host.profile = GRCF_PF_1;
    hdr_host.payload_size = payload_size;
    hdr_host.key_area_size = fob.key_area_size;
    hdr_host.key_offset = fob.key_offset;
    hdr_host.pad_offset = fob.pad_offset;

    store_header(hdr_le, &hdr_host);
    memset(hdr_le->reserved, 0, sizeof(hdr_le->reserved));

    ret = enc_file(payload, payload_size,
                   pt_addr, pt_size,
                   fob.data + fob.key_offset, GRCF_SYM_KEY_SIZE);
    if (ret != 0) goto out;

    /* Stage 2: update stage1 in place (XOR & padding) */
    ret = enc_perform_st2(&fob, payload, payload_size, padding_size);
    if (ret != 0) goto out;

    /* Stage 3: stage1(2) -> dst mapping */
    ret = write_map_file_start(&dstm, path_out, dst_size);
    if (ret != 0) goto out;

    ret = enc_file_pass(dstm.addr, dstm.size,
                        stage1, stage1_size,
                        auth->passwd, auth->passwd_len,
                        fob.data + fob.pepper_offset, GRCF_SYM_KEY_SIZE);
    if (ret != 0) goto out;

    ret = write_map_finish(&dstm, 0);

out:
    errsv = errno;

    write_map_finish(&dstm, ret);
    if (stage1 != NULL) {
        grcf_explicit_bzero(stage1, stage1_size);
        munmap(stage1, stage1_size);
    }
    grcf_explicit_bzero(&hdr_host, sizeof(hdr_host));
    grcf_fob_unmap(&fob);

    errno = errsv;

    return ret;
}

static int dec_compute_st1_size(size_t *st1_size, size_t src_size)
{
    *st1_size = from_sict_size(src_size);
    if (*st1_size == (size_t)-1) return STATUS_DECRYPT_BAD_SRC_SIZE;
    if (*st1_size <= sizeof(struct grcf_ct_header)) return STATUS_DECRYPT_BAD_SRC_SIZE;

    return 0;
}

static int dec_compute_pt_size(size_t *pt_size, size_t stage1_size, size_t payload_size)
{
    if (stage1_size <= payload_size ||
        stage1_size < sizeof(struct grcf_ct_header) + payload_size)
    {
        return STATUS_DECRYPT_BAD_PAYLOAD_SIZE;
    }

    *pt_size = from_ict_size(payload_size);
    if (*pt_size == (size_t)-1) {
        return STATUS_DECRYPT_BAD_PAYLOAD_SIZE;
    }

    return 0;
}

/* Size remaining for edits in the top page before overmapping an extra page
 * initially */
#define OVERMAP_THRESHOLD   512

/* Decide how much plaintext space to map based on the actual data size
 * in the case overmapping is desired for potential in-place editing
 * (when serving RW memory mappings)
 *
 * Return:
 *  A page-aligned value greater than original size, on success.
 *  Original size on error.
 */
static size_t dec_compute_pt_overmapped_size(size_t size)
{
    size_t  page_size = 0;
    size_t  mapped_size = 0;

    page_size = sysconf(_SC_PAGESIZE);
    if (page_size == (size_t)-1) return size;

    mapped_size = (1 + size / page_size) * page_size;
    if (size % page_size + OVERMAP_THRESHOLD > page_size) {
        mapped_size += page_size;
    }

    return mapped_size;
}

int dec_3stage(char const *path_in,
               char const *path_out,
               struct grcf_auth const *auth,
               int overmap,
               struct grcf_mmapping *ptm,
               int fd_shm)
{
    int                         ret = 0;
    int                         errsv = 0;
    struct grcf_key_fob         fob;

    uint8_t                     *src = NULL;
    size_t                      src_size = 0;
    uint8_t                     *stage1 = NULL;
    size_t                      stage1_size = 0;
    struct grcf_ct_header       *hdr_le = NULL;
    struct grcf_ct_header_host  hdr_host;
    uint8_t                     *payload = NULL;
    size_t                      payload_size = 0;
    uint8_t                     *pt = NULL;
    size_t                      pt_size = 0;
    size_t                      pt_mapped_size = 0;
    struct grcf_wfile_mmapping  dstm;

    /* XXX refactor? */
    if (path_out != NULL) {
        if (ptm != NULL || fd_shm >= 0) return STATUS_CE_DEC3ST_BOTH_FILE_MAPPING_GIVEN;
        /* Writing to a file */
    } else if (fd_shm >= 0) {
        if (ptm != NULL) return STATUS_CE_DEC3ST_BOTH_FILE_MAPPING_GIVEN;
    } else {
        if (ptm == NULL) return STATUS_CE_DEC3ST_BOTH_FILE_MAPPING_MISSING;
        /* Serving a mapping */
        memset(ptm, 0, sizeof(*ptm));
    }

    memset(&fob, 0, sizeof(fob));
    memset(&hdr_host, 0, sizeof(hdr_host));
    memset(&dstm, 0, sizeof(dstm));

    ret = read_map_file(&src, &src_size, path_in, 0);
    if (ret != 0) goto out;

    ret = grcf_fob_map(&fob, auth->path_key, auth->passwd, auth->passwd_len);
    if (ret != 0) goto out;

    /* Stage 3: src -> stage1 */
    ret = dec_compute_st1_size(&stage1_size, src_size);
    if (ret != 0) goto out;

    ret = rw_map_anon(&stage1, stage1_size);
    if (ret != 0) goto out;

    ret = dec_file_pass(stage1, stage1_size,
                        src, src_size,
                        auth->passwd, auth->passwd_len,
                        fob.data + fob.pepper_offset, GRCF_SYM_KEY_SIZE);
    if (ret != 0) goto out;

    munmap(src, src_size);
    src = NULL;
    src_size = 0;

    /* Stage 2: update stage1 in place */
    hdr_le = (struct grcf_ct_header*)stage1;
    load_header(&hdr_host, hdr_le);

    if (hdr_host.version != GRCF_3ST_HEADER_VERSION) {
        ret = STATUS_DECRYPT_INVALID_VERSION;
        goto out;
    }
    if (hdr_host.profile != GRCF_PF_1) {
        ret = STATUS_DECRYPT_INVALID_PROFILE;
        goto out;
    }
    payload_size = hdr_host.payload_size;

    /* Also validate payload_size */
    ret = dec_compute_pt_size(&pt_size, stage1_size, payload_size);
    if (ret != 0) goto out;

    payload = stage1 + sizeof(struct grcf_ct_header);

    ret = grcf_fob_init(&fob, hdr_host.key_area_size,
                       hdr_host.key_offset, hdr_host.pad_offset,
                       hdr_le->fob_pt_digest, sizeof(hdr_le->fob_pt_digest));
    if (ret != 0) goto out;

    xor_st1_payload(&fob, payload, payload_size);

    /* Stage 1: payload -> pt */
    if (ptm == NULL) {
        ret = path_out != NULL ?
            write_map_file_start(&dstm, path_out, pt_size) : /* Writing to a file */
            write_map_shm_start(&dstm, fd_shm, pt_size);     /* Serving a shared mapping */
        if (ret != 0) goto out;

        pt = dstm.addr;
    } else {
        /* Serving a memory mapping */
        pt_mapped_size = overmap ? dec_compute_pt_overmapped_size(pt_size) : pt_size;
        ret = rw_map_anon(&pt, pt_mapped_size);
        if (ret != 0) goto out;

        ptm->addr = pt;
        ptm->size = pt_size;
        ptm->size_mapped = pt_mapped_size;
    }

    ret = dec_file(pt, pt_size,
                   payload, payload_size,
                   fob.data + fob.key_offset, GRCF_SYM_KEY_SIZE);
    if (ret != 0) goto out;

    ret = write_map_finish(&dstm, 0);  /* No-op if not started */

out:
    errsv = errno;

    write_map_finish(&dstm, ret);

    if (ret != 0 && ptm != NULL) {
        unmap_grcf_mapping(ptm, 1);  /* purge any data written */
    }

    grcf_fob_unmap(&fob);
    grcf_explicit_bzero(&hdr_host, sizeof(hdr_host));
    if (stage1 != NULL) {
        grcf_explicit_bzero(stage1, stage1_size);
        munmap(stage1, stage1_size);
    }
    if (src != NULL) {
        munmap(src, src_size);
    }

    errno = errsv;

    return ret;
}

int enc_dec_file(struct grcf_args *pa)
{
    int                     ret = 0;
    int                     errsv = 0;
    uint8_t                 *pt_addr = NULL;
    size_t                  pt_size = 0;

    if (pa->mode == GRCF_MODE_PASS || pa->mode == GRCF_MODE_FULL) {
        ret = read_passwd(pa);
        if (ret != 0) return ret;
    }

    if (pa->cmd == GRCF_CMD_ENC) {
        switch (pa->mode) {
            case GRCF_MODE_KEY:
            case GRCF_MODE_PASS:
                return enc_dec_file_1stage(pa->path_in, pa->path_out,
                                           pa->cmd, pa->mode, &pa->auth);
            case GRCF_MODE_FULL:
                ret = read_map_file(&pt_addr, &pt_size, pa->path_in, 0);
                if (ret != 0) return ret;
                ret = enc_3stage(pa->path_out, &pa->auth,
                                 pa->flags & GRCF_ARGS_F_PPAGE_SIZE ? pa->ppage_size : (size_t)-1,
                                 pt_addr, pt_size);
                errsv = errno;
                munmap(pt_addr, pt_size);
                errno = errsv;
                return ret;
            default:
                return STATUS_INVALID_MODE;
        }
    } else if (pa->cmd == GRCF_CMD_DEC) {
        switch (pa->mode) {
            case GRCF_MODE_KEY:
            case GRCF_MODE_PASS:
                return enc_dec_file_1stage(pa->path_in, pa->path_out,
                                           pa->cmd, pa->mode, &pa->auth);
            case GRCF_MODE_FULL:
                return dec_3stage(pa->path_in, pa->path_out, &pa->auth,
                                  0, NULL, -1);     /* no overmap, no output mapping requested */
            default:
                return STATUS_INVALID_MODE;
        }
    }

    return STATUS_INVALID_COMMAND;
}
