/*
 * Copyright (c) 2021-2022 Denis Remezov
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#ifndef GRCF_FILE_CRYPT_H_INCLUDED
#define GRCF_FILE_CRYPT_H_INCLUDED

#include "common.h"
#include "util.h"

#include <stdint.h>
#include <stddef.h>

/* High-level symmetric block cipher encryption and decryption for files */

/* Padding page minimum and maximum sizes
 * Must be a power of 2.
 *
 * Note that the actual padding is a combination of deterministic and
 * randomized parts.
 */
#define GRCF_PADDING_PAGE_SIZE_MIN      0x0400
#define GRCF_PADDING_PAGE_SIZE_MAX      (0x40 * 0x100000)

/* 3-stage protection scheme version */
#define GRCF_3ST_HEADER_VERSION         3

/* Crypto profile
 * The profile value is stored in stage 1/2 header, and it describes the
 * algorithms used for processing and padding stage 1/2 payload and the
 * resulting layout.
 * Note that this does not cover stage 3 algorithms, which are fixed in the
 * current design.
 */
enum grcf_profile {
    GRCF_PF_INVALID                     = 0,
    /* Profile 1:
     * Fob digest:  SHA-256 over fob plaintext
     * Stage 1:     AES-256-GCM with a fob key (IV(12)-CT-TAG(16) layout)
     * Stage 2:     XOR with key fob material & random padding
     */
    GRCF_PF_1                           = 1
};

/* Perform 3-stage file encryption with authentication
 *
 * ppage_size   padding page size (see DESIGN)
 * pt_addr      plaintext address
 * pt_size      plaintext size
 *
 * If supplied ppage_size is -1 then the effective padding page size is chosen
 * heuristically based on the source file size.
 *
 * This call is similar to enc_dec_file() for GRCF_MODE_FULL.
 * The caller is responsible for plaintext management.
 */
int enc_3stage(char const *path_out,
               struct grcf_auth const *auth,
               size_t ppage_size,
               uint8_t const *pt_addr,
               size_t pt_size);

/* Perform 3-stage file decryption with authentication
 *
 * One and only one of {path_out, ptm, fd_shm} must be provided.
 * XXX Perhaps get rid of the ptm option (used for FUSE mounting only).
 *
 * - If path_out != NULL, the mapping is backed by the output file and is
 * released internally.
 * - If fd_shm is >= 0, a shared memory mapping is created based on fd_shm and
 * is released internally.  Used for IPC.
 * - If ptm != NULL then a destination memory mapping is created and set on
 * ptm, which on success the caller must eventually release
 * (either with unmap_grcf_mapping()  or as a result of enc_3stage_unmap()).
 * This option is used for serving FUSE mounts.
 *
 * auth: credentials for decryption and authnetication.
 * overmap: reserve space for editing (only when ptm != NULL)
 *
 * Return 0 on success; STATUS_... on error
 */
int dec_3stage(char const *path_in,
               char const *path_out,
               struct grcf_auth const *auth,
               int overmap,
               struct grcf_mmapping *ptm,
               int fd_shm);

/* Encrypt or decrypt a file with authentication
 *
 * Both encryption and decryption have the following modes of operation:
 * - GRCF_MODE_KEY:
 *   One-stage encryption scheme.
 *   Encryption: encrypt an input file and compute an authentication tag using
 *   the supplied key.  Internally, generate a randoom 12-byte nonce.
 *
 * - GRCF_MODE_PASS:
 *   One-stage encryption scheme.
 *   Encryption: encrypt an input file and compute an authentication tag using a
 *   key derived from a password (prompted) and a randomly generated 32-byte salt.
 *   Internally, generate a randoom 12-byte nonce.
 *
 * - GRCF_MODE_FULL:
 *   Three-stage encryption scheme.
 *   Perform two-factor (password-based and key fob-based) encrytion and
 *   authentication with XOR-ing in a key pad and padding the output.
 *   Encryption: if page size is not provided (GRCF_ARGS_F_PPAGE_SIZE not set),
 *   it will be computed heuristically from size.
 *
 * Decryption (all modes): authenticate the ciphertext and effectively reverse
 * the encryption process.
 *
 * Return 0 on success; STATUS_... on error
 */
int enc_dec_file(struct grcf_args *pa);

#endif
