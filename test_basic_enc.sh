#!/bin/sh

. ./test_basic_common.sh

test_enc_full()
{
    local path_e="$path_i".enc_f
    local passwd="$1"

    echo "$passwd" | ./grcf enc -i "$path_i" -o "$path_e" -c full -k "$path_fob"
    if [ $? -ne 0 ]; then
        printf "Encryption failed\n"
        rc=11
        return
    fi
}

test_enc_pass()
{
    local path_e="$path_i".enc_p
    local passwd="$1"

    echo "$passwd" | ./grcf enc -i "$path_i" -o "$path_e" -c pass
    if [ $? -ne 0 ]; then
        printf "Encryption failed\n"
        rc=21
        return
    fi
}

test_enc_key()
{
    local path_e="$path_i".enc_k

    ./grcf enc -i "$path_i" -o "$path_e" -c key -k "$path_key"
    if [ $? -ne 0 ]; then
        printf "Encryption failed\n"
        rc=31
        return
    fi
}

gen_key_file()
{
    path_key="$TEST_DIR"/key
    if [ -f "$path_key" ]; then
        echo "Using existing key"
        return
    fi

    dd if=/dev/urandom of="$path_key" bs="$KEY_SIZE" count=1
    rc=$?
}

gen_fob_file()
{
    path_fob="$TEST_DIR"/fob
    if [ -f "$path_fob" ]; then
        echo "Using existing fob"
        return
    fi

    ./grcf fob -o "$path_fob" -s "$FOB_SIZE"
    rc=$?
}

gen_data_file()
{
    path_i="$TEST_DIR"/data_"$size"
    if [ -f "$path_i" ]; then
        printf "Using existing data file %s\n" "$path_i"
        return
    fi

    dd if=/dev/urandom of="$path_i" bs=1 count="$size"
    rc=$?
}

do_main()
{
    mkdir -p "$TEST_DIR"

    gen_key_file
    [ "$rc" -eq 0 ] || return
    gen_fob_file
    [ "$rc" -eq 0 ] || return

    for size in $*; do
        printf "Processing file size %d\n" "$size"

        gen_data_file
        [ "$rc" -eq 0 ] || return

        test_enc_full "$TEST_PASSWD"
        [ $rc -eq 0 ] || return

        test_enc_pass "$TEST_PASSWD"
        [ "$rc" -eq 0 ] || return

        test_enc_key
        [ "$rc" -eq 0 ] || return
    done
}

do_main "$FILE_SIZES"

exit "$rc"
