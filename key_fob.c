/*
 * Copyright (c) 2021-2022 Denis Remezov
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include "crypto_dgst.h"
#include "crypto_pbkdf.h"
#include "crypto_symciph.h"
#include "crypto_util.h"
#include "key_fob.h"
#include "status_codes.h"
#include "util.h"

#include <errno.h>
#include <string.h>
#include <sys/mman.h>
#include <unistd.h>


/* Byte offset of stage 3 pepper within plaintext fob */
#define FOB_PEPPER_OFFS     0

/* Byte offset of stage 1 key area within plaintext fob */
#define FOB_KEY_AREA_OFFS   (FOB_PEPPER_OFFS + GRCF_SYM_KEY_SIZE)

/* Plaintext fobs are aligned at 8 bytes */
#define FOB_PT_SIZE_MASK    (~(sizeof(uint64_t) - 1))

/* Key fob plaintext layout:
 *      FIELD           SIZE
 *      ---------------------
 *      Pepper          32
 *      Key Area        n_keys * 32
 *      Pad Area        fob_size - (32 + n_keys * 32)
 */

int grcf_is_fob_size_valid(size_t size)
{
    return size >= GRCF_FOB_FILE_SIZE_MIN && size <= GRCF_FOB_FILE_SIZE_MAX;
}

int grcf_fob_create(char const *path_fob, size_t size)
{
    int                         ret = 0;
    struct grcf_wfile_mmapping  dstm;

    if (!grcf_is_fob_size_valid(size)) return STATUS_INVALID_KEY_FOB_FILE_SIZE;

    memset(&dstm, 0, sizeof(dstm));

    ret = write_map_file_start(&dstm, path_fob, size);
    if (ret != 0) goto out;

    ret = grcf_random(dstm.addr, size);
    if (ret != 0) goto out;

    ret = write_map_finish(&dstm, 0);

out:
    write_map_finish(&dstm, ret);
    return ret;
}

int grcf_fob_map(struct grcf_key_fob *fob, char const *path_fob,
                 char const *passwd, size_t passwd_len)
{
    int                     ret = 0;
    int                     errsv = 0;
    uint8_t                 key_pass[GRCF_SYM_KEY_SIZE];
    uint8_t                 *fob_ct = NULL;
    uint8_t                 *fob_pt = NULL;
    size_t                  fob_size_ct = 0;
    size_t                  fob_size_pt = 0;
    uint8_t const           *salt = NULL;
    uint8_t const           *iv = NULL;
    size_t const            ct_offset = GRCF_PBKDF_SALT_SIZE + GRCF_CTR_IV_SIZE;

    memset(fob, 0, sizeof(*fob));

    ret = read_map_file(&fob_ct, &fob_size_ct, path_fob, 0);
    if (ret != 0) return ret;

    if (!grcf_is_fob_size_valid(fob_size_ct)) {
        ret = STATUS_INVALID_KEY_FOB_FILE_SIZE;
        goto out;
    }

    /* The following is safe due to grcf_is_fob_size_valid() above */
    fob_size_pt = (fob_size_ct & FOB_PT_SIZE_MASK) - ct_offset;

    ret = rw_map_anon(&fob_pt, fob_size_pt);
    if (ret != 0) goto out;

    if (((uintptr_t)fob_pt % sizeof(uint64_t)) != 0) {
        ret = STATUS_UNALIGNED_MAPPING;
        goto out;
    }

    salt = fob_ct;
    iv = fob_ct + GRCF_PBKDF_SALT_SIZE;
    ret = grcf_pbkdf(key_pass, sizeof(key_pass),
                     passwd, passwd_len,
                     salt, GRCF_PBKDF_SALT_SIZE);
    if (ret != 0) goto out;

    ret = grcf_crypt_ctr(fob_pt, fob_size_pt,
                         fob_ct + ct_offset, fob_size_pt,
                         key_pass, sizeof(key_pass),
                         iv, GRCF_CTR_IV_SIZE);
    if (ret != 0) goto out;

    fob->data = fob_pt;
    fob->size = fob_size_pt;
    fob->pepper_offset = FOB_PEPPER_OFFS;

out:
    errsv = errno;
    grcf_explicit_bzero(key_pass, sizeof(key_pass));
    if (ret != 0) {
        memset(fob, 0, sizeof(*fob));
        if (fob_pt != NULL) {
            grcf_explicit_bzero(fob_pt, fob_size_pt);
            munmap(fob_pt, fob_size_pt);
        }
    }
    if (fob_ct != NULL) {
        munmap(fob_ct, fob_size_ct);
    }
    errno = errsv;

    return ret;
}

/* Validate and set key_area_size, initialize pad area fields */
static int fob_init_common(struct grcf_key_fob *fob, size_t key_area_size)
{
    size_t          pad_area_offset = 0;

    if (key_area_size < GRCF_N_FOB_KEYS_MIN * GRCF_SYM_KEY_SIZE ||
        key_area_size > fob->size - FOB_KEY_AREA_OFFS - GRCF_PAD_AREA_SIZE_MIN ||
        (key_area_size % GRCF_SYM_KEY_SIZE) != 0)
    {
        return STATUS_BAD_KEY_AREA_SIZE;
    }

    fob->key_area_size = key_area_size;

    pad_area_offset = FOB_KEY_AREA_OFFS + key_area_size;
    fob->pad_area_size = fob->size - pad_area_offset;
    fob->pad_area = fob->data + pad_area_offset;

    return 0;
}

/* Choose a partitioning of a fob based on its size.
 * This is policy-level logic.
 * Note that the return may be out of range if fob_size is bad
 * (if this happened it would be dealt with during fob_init_common(),
 * but since the fob is assumed valid, this would be unexpected).
 */
static size_t choose_key_area_size(size_t fob_size)
{
    /* Start with a minimum number of keys, then split the rest between the
     * pad, more keys and a single pepper */
    size_t  size = GRCF_N_FOB_KEYS_MIN * GRCF_SYM_KEY_SIZE +
           ((fob_size - FOB_KEY_AREA_OFFS - GRCF_N_FOB_KEYS_MIN * GRCF_SYM_KEY_SIZE) >> 3);
    /* Trim the key area size to align */
    return size - (size % GRCF_SYM_KEY_SIZE);
}

int grcf_fob_init_new(struct grcf_key_fob *fob, uint8_t *dgst, size_t dgst_size)
{
    int         ret = 0;
    uint64_t    r = 0;
    size_t      key_area_size = choose_key_area_size(fob->size);
    size_t      n_elem = 0;

    ret = grcf_sha256(dgst, dgst_size, fob->data, fob->size);
    if (ret != 0) return ret;

    ret = fob_init_common(fob, key_area_size);
    if (ret != 0) return ret;

    /* Pick a key (for stage 1) and a pad offset (for stage 2) at random */
    ret = grcf_random_u64(&r);
    if (ret != 0) return ret;
    n_elem = key_area_size / GRCF_SYM_KEY_SIZE;
    fob->key_offset = FOB_KEY_AREA_OFFS + (r % n_elem) * GRCF_SYM_KEY_SIZE;

    ret = grcf_random_u64(&r);
    if (ret != 0) return ret;
    n_elem = fob->pad_area_size / sizeof(uint64_t);
    fob->pad_offset = (r % n_elem) * sizeof(uint64_t);

    return 0;
}

int grcf_fob_init(struct grcf_key_fob *fob,
                  size_t key_area_size, size_t key_offset, size_t pad_offset,
                  uint8_t const *dgst, size_t dgst_size)
{
    int         ret = 0;
    uint8_t     fob_dgst[GRCF_FOB_DGST_SIZE];

    /* Verify the hash */
    if (dgst_size != GRCF_FOB_DGST_SIZE) return STATUS_INVALID_FOB_DGST_SIZE;

    ret = grcf_sha256(fob_dgst, sizeof(fob_dgst), fob->data, fob->size);
    if (ret != 0) return ret;

    if (memcmp(dgst, fob_dgst, GRCF_FOB_DGST_SIZE) != 0) {
        return STATUS_FOB_DGST_MISMATCH;
    }

    ret = fob_init_common(fob, key_area_size);
    if (ret != 0) return ret;

    if (key_offset < FOB_KEY_AREA_OFFS ||
        key_offset >= FOB_KEY_AREA_OFFS + key_area_size ||
        (key_offset % GRCF_SYM_KEY_SIZE) != 0)
    {
        return STATUS_BAD_KEY_OFFSET;
    }
    fob->key_offset = key_offset;

    /* Note that a pad can begin at any position (aligned at 8) within the
     * pad area since it's used in a ring buffer fashion */
    if (pad_offset >= fob->pad_area_size ||
        (pad_offset % sizeof(uint64_t)) != 0)
    {
        return STATUS_BAD_PAD_OFFSET;
    }
    fob->pad_offset = pad_offset;

    return 0;
}

int grcf_fob_unmap(struct grcf_key_fob *fob)
{
    int             ret = 0;

    if (fob != NULL && fob->data != NULL) {
        grcf_explicit_bzero(fob->data, fob->size);
        ret = munmap(fob->data, fob->size);
    }

    memset(fob, 0, sizeof(*fob));
    return ret;
}
