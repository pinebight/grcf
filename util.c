/*
 * Copyright (c) 2021-2022 Denis Remezov
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include "status_codes.h"
#include "util.h"

#include <errno.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>


int grcf_setup_common(void)
{
    /* Enforce mode 0600  mode when using mkstemp() (may be needed
     * on some old systems). */
    umask(S_IXUSR | S_IRWXG | S_IRWXO);
    return 0;
}

int is_pow_2(size_t v)
{
    size_t  lsb = 0;

    for (lsb = 1; lsb != 0; lsb <<= 1) {
        if (lsb & v) return lsb == v;
    }
    return 0;
}

void store_le32(uint8_t (*v)[sizeof(uint32_t)], uint32_t h)
{
    (*v)[0] = h & 0xff;
    h >>= 8;
    (*v)[1] = h & 0xff;
    h >>= 8;
    (*v)[2] = h & 0xff;
    h >>= 8;
    (*v)[3] = h & 0xff;
}

void store_le64(uint8_t (*v)[sizeof(uint64_t)], uint64_t h)
{
    (*v)[0] = h & 0xff;
    h >>= 8;
    (*v)[1] = h & 0xff;
    h >>= 8;
    (*v)[2] = h & 0xff;
    h >>= 8;
    (*v)[3] = h & 0xff;
    h >>= 8;
    (*v)[4] = h & 0xff;
    h >>= 8;
    (*v)[5] = h & 0xff;
    h >>= 8;
    (*v)[6] = h & 0xff;
    h >>= 8;
    (*v)[7] = h & 0xff;
}

uint32_t load_le32(uint8_t const (*v)[sizeof(uint32_t)])
{
    return (uint32_t)(*v)[0] |
           ((uint32_t)(*v)[1] << 8) |
           ((uint32_t)(*v)[2] << 16) |
           ((uint32_t)(*v)[3] << 24);
}

uint64_t load_le64(uint8_t const (*v)[sizeof(uint64_t)])
{
    return (uint64_t)(*v)[0] |
           ((uint64_t)(*v)[1] << 8) |
           ((uint64_t)(*v)[2] << 16) |
           ((uint64_t)(*v)[3] << 24) |
           ((uint64_t)(*v)[4] << 32) |
           ((uint64_t)(*v)[5] << 40) |
           ((uint64_t)(*v)[6] << 48) |
           ((uint64_t)(*v)[7] << 56);
}

int grcf_mlock(const void *addr, size_t size, int error_status)
{
#if defined(GRCF_USE_MLOCK)
    return  mlock(addr, size) == 0 ? 0 : error_status;
#else
    return 0;
#endif
}

int read_map_file(uint8_t **p, size_t* size, char const *path, int lock)
{
    int             ret = 0;
    int             errsv = 0;
    int             fd = -1;
    struct stat     st;
    void            *addr = MAP_FAILED;

    fd = open(path, O_RDONLY);
    if (fd < 0) return STATUS_OPEN_RD_FAILED;

    ret = fstat(fd, &st);
    if (ret != 0) {
        ret = STATUS_STAT_FAILED;
        goto out;
    }

    if (st.st_size == 0) {
        *p = NULL;
        *size = 0;
        goto out;
    }

    addr = mmap(NULL, st.st_size, PROT_READ, MAP_SHARED, fd, 0);
    if (addr == MAP_FAILED) {
        ret = STATUS_MMAP_RD_FAILED;
        goto out;
    }

    if (lock) {
        ret = grcf_mlock(addr, st.st_size, STATUS_MLOCK_RD_FAILED);
        if (ret != 0) goto out;
    }

    *p = addr;
    *size = st.st_size;

out:
    errsv = errno;
    if (fd >= 0) {
        close(fd);
    }
    if (ret != 0 && addr != MAP_FAILED) {
        munmap(addr, st.st_size);
    }
    errno = errsv;

    return ret;
}

int rw_map_anon(uint8_t **p, size_t size)
{
    int             ret = 0;
    int             errsv = 0;
    void            *addr = MAP_FAILED;

    if (size == 0) {
        *p = NULL;
        goto out;
    }

    addr = mmap(NULL, size, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANON, -1, 0);
    if (addr == MAP_FAILED) {
        ret = STATUS_MMAP_WR_FAILED;
        goto out;
    }

    ret = grcf_mlock(addr, size, STATUS_MLOCK_WR_FAILED);
    if (ret != 0) goto out;

    *p = addr;

out:
    errsv = errno;
    if (ret != 0 && addr != MAP_FAILED) {
        munmap(addr, size);
    }
    errno = errsv;

    return ret;
}

int write_map_file_finish(struct grcf_wfile_mmapping *wfm, int status);
int write_map_shm_finish(struct grcf_wfile_mmapping *wfm, int status);

int write_map_file_start(struct grcf_wfile_mmapping *wfm,
                         char const *path, size_t size)
{
    int             ret = 0;
    int             errsv = 0;
    int             fd = -1;
    void            *addr = MAP_FAILED;
    char const      templ_suf[] = ".tmp.XXXXXX";
    size_t          tmp_path_size = strlen(path);

    wfm->addr = NULL;
    wfm->size = 0;
    wfm->dtor = NULL;
    wfm->path = path;
    wfm->tmp_path = NULL;

    if (tmp_path_size > SIZE_MAX - sizeof(templ_suf)) return STATUS_PATH_TOO_LONG;
    tmp_path_size += sizeof(templ_suf);

    wfm->tmp_path = malloc(tmp_path_size);
    if (wfm->tmp_path == NULL) return STATUS_OOM;

    strcpy(wfm->tmp_path, wfm->path);
    strcat(wfm->tmp_path, templ_suf);

    fd = mkstemp(wfm->tmp_path);
    if (fd < 0) {
        /* We'll know the file has been created iff tmp_path != NULL */
        free(wfm->tmp_path);
        wfm->tmp_path = NULL;
        ret = STATUS_MKSTEMP_FAILED;
        goto out;
    }

    ret = ftruncate(fd, size);
    if (ret != 0) {
        ret = STATUS_FTRUNCATE_FAILED;
        goto out;
    }

    if (size != 0) {
        addr = mmap(NULL, size, PROT_WRITE, MAP_SHARED, fd, 0);
        if (addr == MAP_FAILED) {
            ret = STATUS_MMAP_WR_FAILED;
            goto out;
        }
    }

    wfm->addr = addr;
    wfm->size = size;
    wfm->dtor = write_map_file_finish;

out:
    errsv = errno;
    if (fd >= 0) {
        close(fd);
    }
    if (ret != 0) {
        write_map_file_finish(wfm, ret);
    }
    errno = errsv;

    return ret;
}

int write_map_shm_start(struct grcf_wfile_mmapping *wfm, int fd, size_t pld_size)
{
    int             ret = 0;
    size_t          mapped_size = sizeof(struct grcf_shm_header) + pld_size;
    struct grcf_shm_header *shm = MAP_FAILED;

    wfm->addr = NULL;
    wfm->size = 0;
    wfm->dtor = NULL;
    wfm->shm = MAP_FAILED;

    ret = ftruncate(fd, mapped_size);
    if (ret != 0) return STATUS_FTRUNCATE_FAILED;

    shm = mmap(NULL, mapped_size, PROT_WRITE, MAP_SHARED, fd, 0);
    if (shm == MAP_FAILED) return STATUS_MMAP_WR_FAILED;

    shm->size = pld_size;
    shm->size_mapped = mapped_size;

    wfm->addr = (uint8_t *)(shm + 1);
    wfm->size = pld_size;
    wfm->shm = shm;
    wfm->dtor = write_map_shm_finish;

    return 0;
}

int write_map_finish(struct grcf_wfile_mmapping *wfm, int status)
{
    int     ret = 0;

    if (wfm->dtor != NULL) {
        ret = wfm->dtor(wfm, status);
        wfm->dtor = NULL;
    }
    return ret;
}

int write_map_file_finish(struct grcf_wfile_mmapping *wfm, int status)
{
    int             ret = 0;
    int             errsv = errno;

    if (wfm->addr != NULL) {
        munmap(wfm->addr, wfm->size);
        wfm->addr = NULL;
    }
    if (wfm->tmp_path != NULL) {
        if (status == 0) {
            if (rename(wfm->tmp_path, wfm->path) != 0) {
                errsv = errno;
                ret = STATUS_RENAME_FAILED;
            }
        } else {
            unlink(wfm->tmp_path);
        }

        free(wfm->tmp_path);
        wfm->tmp_path = NULL;
    }
    errno = errsv;

    return ret;
}

int write_map_shm_finish(struct grcf_wfile_mmapping *wfm, int status)
{
    if (wfm->addr != NULL) {
        if (status != 0) {
            grcf_explicit_bzero(wfm->shm, wfm->shm->size_mapped);
        }
        munmap(wfm->shm, wfm->shm->size_mapped);

        wfm->addr = NULL;
        wfm->size = 0;
        wfm->shm = MAP_FAILED;
    }

    return 0;
}

void unmap_grcf_mapping(struct grcf_mmapping *ptm, int purge)
{
    int                     errsv = errno;

    if (ptm->addr != NULL) {
        if (purge) {
            grcf_explicit_bzero(ptm->addr, ptm->size_mapped);
        }
        munmap(ptm->addr, ptm->size_mapped);
    }
    memset(ptm, 0, sizeof(*ptm));

    errno = errsv;
}

int is_dir(char const *path)
{
    int             ret = 0;
    struct stat     st;

    ret = stat(path, &st);
    return ret == 0 && (st.st_mode & S_IFMT) == S_IFDIR;
}

int is_valid_fname(char const *name)
{
    return name != NULL &&
           strlen(name) > 0 &&
           strcmp(name, ".") != 0 &&
           strcmp(name, "..") != 0 &&
           strchr(name, '/') == NULL;
}

int canonicalize_base_dir(char **path_out, char const *path_in)
{
    size_t      len_dir = 0;
    size_t      len_name = 0;
    size_t      size_max = 0;
    char        *buf_dir = NULL;
    char        *buf_path = NULL;
    char        *p = NULL;
    char const  *name = NULL;

    *path_out = NULL;

    if (path_in == NULL) return STATUS_PATH_INVALID;

    if (is_valid_fname(path_in)) {
        /* path_in is a file name with no base directory */
        /* Need to construct base_dir/name\0 from CWD and path_in */
        name = path_in;
        len_name = strlen(name);
        size_max = PATH_MAX + 1 + len_name;
        buf_path = calloc(size_max, 1);
        if (buf_path == NULL) return STATUS_OOM;

        /* base_dir\0 */
        if (getcwd(buf_path, PATH_MAX) == NULL) {
            free(buf_path);
            return STATUS_GETCWD_FAILED;
        }
    } else {
        p = strrchr(path_in, '/');
        if (p == NULL || !is_valid_fname(p + 1)) return STATUS_PATH_INVALID;

        /* path_in is a path with a base directory */
        /* Need to canonicalize the base directory only */
        len_dir = p - path_in;
        buf_dir = strndup(path_in, len_dir);
        if (buf_dir == NULL) return STATUS_OOM;

        name = p + 1;
        len_name = strlen(name);
        size_max = PATH_MAX + 1 + len_name;
        buf_path = calloc(size_max, 1);
        if (buf_path == NULL) {
            free(buf_dir);
            return STATUS_OOM;
        }
        if (realpath(buf_dir, buf_path) == NULL) {
            free(buf_path);
            free(buf_dir);
            return STATUS_REALPATH_FAILED;
        }
        free(buf_dir);
    }

    len_dir = strlen(buf_path);
    buf_path[len_dir] = '/';
    strcpy(buf_path + len_dir + 1, name);
    *path_out = buf_path;

    return 0;
}
