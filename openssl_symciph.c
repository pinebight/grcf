/*
 * Copyright (c) 2021-2022 Denis Remezov
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include "crypto_symciph.h"
#include "status_codes.h"

#include <limits.h>
#include <openssl/evp.h>
#include <string.h>


/* See openssl/opensslv.h for OPENSSL_VERSION_NUMBER format.
 *
 * EVP_EncryptFinal_ex() fails in OpenSSL 1.1.1d and LibreSSL (as recent
 * as 3.4.1) when calling EVP_EncryptUpdate() with 0 size data.
 * OpenSSL >= 1.1.1f doesn't complain.
 */
#if (OPENSSL_VERSION_NUMBER < 0x1010106fL) || defined(LIBRESSL_VERSION_NUMBER)
#define OSSL_AVOID_ENC_ZERO_LEN_BUF
#endif

static int alloc_ctx_gcm(EVP_CIPHER_CTX **ctx, EVP_CIPHER const **cipher)
{
    *ctx = EVP_CIPHER_CTX_new();
    if (*ctx == NULL) return STATUS_CIPHER_CTX_FAILED;

    *cipher = EVP_aes_256_gcm();
    if (*cipher == NULL) return STATUS_AES_256_GCM_FAILED;

    return 0;
}

int grcf_encrypt_auth(uint8_t *dst, size_t dst_size,
                      uint8_t const *src, size_t src_size,
                      uint8_t const *key, size_t key_size,
                      uint8_t const *iv, size_t iv_size)
{
    int                 ret = 0;
    EVP_CIPHER_CTX      *ctx = NULL;
    EVP_CIPHER const    *cipher = NULL;
    int                 part_size = 0;     /* ignored by openSSL on input */

    if (iv_size != GRCF_AEAD_IV_SIZE) return STATUS_INVALID_IV_SIZE;
    if (key_size != GRCF_SYM_KEY_SIZE) return STATUS_INVALID_KEY_SIZE;
    if (dst_size != src_size + GRCF_AUTHTAG_SIZE || dst_size < src_size) {
        return STATUS_CE_INVALID_BUFFER_SIZE;
    }
    /* an int in openSSL API */
    if (src_size > INT_MAX) return STATUS_SIZE_OUT_OF_RANGE;

    ret = alloc_ctx_gcm(&ctx, &cipher);
    if (ret != 0) goto out;

#if 0
    /* This could be used if GRCF_AEAD_IV_SIZE were different from OpenSSL default,
     * which is 12 bytes.  We have no reasons to use a different nonce length
     * except perhaps for testing */
    if (!EVP_EncryptInit_ex(ctx, cipher, NULL, NULL, NULL)) {
        ret = STATUS_ENC_INIT_FAILED;
        goto out;
    }
    if (!EVP_CIPHER_CTX_ctrl(ctx, EVP_CTRL_GCM_SET_IVLEN, iv_size, NULL)) {
        ret = STATUS_SET_IVLEN_FAILED;
        goto out;
    }
    /* Skip the cipher parameter to preserve iv_size as set above */
    if (!EVP_EncryptInit_ex(ctx, NULL, NULL, key, iv)) {
        ret = STATUS_ENC_INIT_FAILED;
        goto out;
    }
#else
    if (!EVP_EncryptInit_ex(ctx, cipher, NULL, key, iv)) {
        ret = STATUS_ENC_INIT_FAILED;
        goto out;
    }
#endif
    /* Disable padding */
    EVP_CIPHER_CTX_set_padding(ctx, 0);

    /* Actual encryption */
#ifdef OSSL_AVOID_ENC_ZERO_LEN_BUF
    if (src_size != 0) {
#endif
        if (!EVP_EncryptUpdate(ctx, dst, &part_size, src, src_size)) {
            ret = STATUS_ENC_UPDATE_FAILED;
            goto out;
        }
#ifdef OSSL_AVOID_ENC_ZERO_LEN_BUF
    }
#endif
    if (part_size != src_size) {
        ret = STATUS_ENC_BAD_UPDATE_SIZE;
        goto out;
    }
    if (!EVP_EncryptFinal_ex(ctx, NULL, &part_size)) {
        ret = STATUS_ENC_FINAL_FAILED;
        goto out;
    }
    if (part_size != 0) {
        ret = STATUS_ENC_BAD_FINAL_SIZE;
        goto out;
    }

    /* Finally put on a tag */
    if (!EVP_CIPHER_CTX_ctrl(ctx,
                             EVP_CTRL_GCM_GET_TAG,
                             GRCF_AUTHTAG_SIZE,
                             dst + src_size))
    {
        ret = STATUS_ENC_GETTAG_FAILED;
        goto out;
    }

out:
    if (ctx != NULL) EVP_CIPHER_CTX_free(ctx);

    return ret;
}

int grcf_decrypt_auth(uint8_t *dst, size_t dst_size,
                      uint8_t const *src, size_t src_size,
                      uint8_t const *key, size_t key_size,
                      uint8_t const *iv, size_t iv_size)
{
    int                 ret = 0;
    EVP_CIPHER_CTX      *ctx = NULL;
    EVP_CIPHER const    *cipher = NULL;
    int                 part_size = 0;     /* ignored by OpenSSL on input */
    uint8_t             tag[GRCF_AUTHTAG_SIZE];

    if (iv_size != GRCF_AEAD_IV_SIZE) return STATUS_INVALID_IV_SIZE;
    if (key_size != GRCF_SYM_KEY_SIZE) return STATUS_INVALID_KEY_SIZE;
    if (dst_size + GRCF_AUTHTAG_SIZE != src_size || src_size < dst_size) {
        return STATUS_CE_INVALID_BUFFER_SIZE;
    }
    /* an int in openSSL API */
    if (dst_size > INT_MAX) return STATUS_SIZE_OUT_OF_RANGE;

    ret = alloc_ctx_gcm(&ctx, &cipher);
    if (ret != 0) goto out;

#if 0
    /* See comments in grcf_encrypt_auth */
    if (!EVP_DecryptInit_ex(ctx, cipher, NULL, NULL, NULL)) {
        ret = STATUS_DEC_INIT_FAILED;
        goto out;
    }
    if (!EVP_CIPHER_CTX_ctrl(ctx, EVP_CTRL_GCM_SET_IVLEN, iv_size, NULL)) {
        ret = STATUS_SET_IVLEN_FAILED;
        goto out;
    }
    /* Skip the cipher parameter to preserve iv_size as set above */
    if (!EVP_DecryptInit_ex(ctx, NULL, NULL, key, iv)) {
        ret = STATUS_DEC_INIT_FAILED;
        goto out;
    }
#else
    if (!EVP_DecryptInit_ex(ctx, cipher, NULL, key, iv)) {
        ret = STATUS_DEC_INIT_FAILED;
        goto out;
    }
#endif
    /* Disable padding */
    EVP_CIPHER_CTX_set_padding(ctx, 0);

    /* Actual decryption */
    if (!EVP_DecryptUpdate(ctx, dst, &part_size, src, dst_size)) {
        ret = STATUS_DEC_UPDATE_FAILED;
        goto out;
    }
    if (part_size != dst_size) {
        ret = STATUS_DEC_BAD_UPDATE_SIZE;
        goto out;
    }

    /* Prepare to authenticate */
    memcpy(tag, src + part_size, sizeof(tag));
    if (!EVP_CIPHER_CTX_ctrl(ctx,
                             EVP_CTRL_GCM_SET_TAG,
                             GRCF_AUTHTAG_SIZE,
                             tag))
    {
        ret = STATUS_DEC_SETTAG_FAILED;
        goto out;
    }

    /* Authenticate */
    if (!EVP_DecryptFinal_ex(ctx, tag, &part_size)) {
        ret = STATUS_DEC_AUTH_FAILED;
        goto out;
    }

out:
    if (ctx != NULL) EVP_CIPHER_CTX_free(ctx);

    return ret;
}

static int alloc_ctx_ctr(EVP_CIPHER_CTX **ctx, EVP_CIPHER const **cipher)
{
    *ctx = EVP_CIPHER_CTX_new();
    if (*ctx == NULL) return STATUS_CIPHER_CTX_FAILED;

    *cipher = EVP_aes_256_ctr();
    if (*cipher == NULL) return STATUS_AES_256_CTR_FAILED;

    return 0;
}

int grcf_crypt_ctr(uint8_t *dst, size_t dst_size,
                   uint8_t const *src, size_t src_size,
                   uint8_t const *key, size_t key_size,
                   uint8_t const *iv, size_t iv_size)
{
    int                 ret = 0;
    EVP_CIPHER_CTX      *ctx = NULL;
    EVP_CIPHER const    *cipher = NULL;
    int                 part_size = 0;     /* ignored by OpenSSL on input */

    if (iv_size != GRCF_CTR_IV_SIZE) return STATUS_INVALID_IV_SIZE;
    if (key_size != GRCF_SYM_KEY_SIZE) return STATUS_INVALID_KEY_SIZE;
    if (dst_size != src_size) return STATUS_CE_INVALID_BUFFER_SIZE;
    /* an int in openSSL API */
    if (src_size > INT_MAX) return STATUS_SIZE_OUT_OF_RANGE;

    ret = alloc_ctx_ctr(&ctx, &cipher);
    if (ret != 0) goto out;

    if (!EVP_EncryptInit_ex(ctx, cipher, NULL, key, iv)) {
        ret = STATUS_DEC_INIT_FAILED;
        goto out;
    }
    /* Disable padding */
    EVP_CIPHER_CTX_set_padding(ctx, 0);

    /* Actual encryption/decryption */
    if (!EVP_EncryptUpdate(ctx, dst, &part_size, src, src_size)) {
        ret = STATUS_ENC_UPDATE_FAILED;
        goto out;
    }
    if (part_size != dst_size) {
        ret = STATUS_ENC_BAD_UPDATE_SIZE;
        goto out;
    }
    if (!EVP_EncryptFinal_ex(ctx, NULL, &part_size)) {
        ret = STATUS_ENC_FINAL_FAILED;
        goto out;
    }
    if (part_size != 0) {
        ret = STATUS_ENC_BAD_FINAL_SIZE;
        goto out;
    }

out:
    if (ctx != NULL) EVP_CIPHER_CTX_free(ctx);

    return ret;
}
