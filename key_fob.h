/*
 * Copyright (c) 2021-2022 Denis Remezov
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#ifndef GRCF_KEY_FOB_H_INCLUDED
#define GRCF_KEY_FOB_H_INCLUDED

#include <stddef.h>
#include <stdint.h>
#include <sys/types.h>

/* Min and max key fob file size (bytes) */
#define GRCF_FOB_FILE_SIZE_MIN      0x1000
#define GRCF_FOB_FILE_SIZE_MAX      (0x400UL * 0x400UL * 0x400UL)

/* Minimum number of keys in a fob */
#define GRCF_N_FOB_KEYS_MIN         20

/* Minimum pad area size in a fob (bytes)
 * Note: ensure that
 * GRCF_SYM_KEY_SIZE + GRCF_N_FOB_KEYS_MIN * GRCF_SYM_KEY_SIZE +
 *      GRCF_PAD_AREA_SIZE_MIN <= GRCF_FOB_FILE_SIZE_MIN
 */
#define GRCF_PAD_AREA_SIZE_MIN      0x400

/* Fob digest size.  The algorithm may in principle depend on the profile;
 * currently fixed at SHA-256.
 * Note that this digest is NOT intended to provide any security - its
 * purpose is to help detect accidental fob corruption.
 */
#define GRCF_FOB_DGST_SIZE          32

/* Key fob in-memory representation
 *
 * During 3-stage encryption or decryption, a key fob provides three pieces:
 * - Stage 1 symmetric key for encryption/decryption
 * - Stage 2 key pad for XOR operation
 * - Stage 3 pepper value to XOR the password-derived key
 *
 * To simplify usage, the following alignment guarantees are provided:
 *  data:               8 bytes
 *  size:               8 bytes
 *  pepper_offset:      32 bytes (symmetric key size)
 *  key_offset:         32 bytes (symmetric key size)
 *  pad_area:           8 bytes
 *  pad_area_size:      8 bytes
 *  pad_offset:         8 bytes
 */
struct grcf_key_fob {
    uint8_t             *data;
    size_t              size;

    /* stage 3 pepper offset from data */
    size_t              pepper_offset;

    /* stage 1 key area size */
    size_t              key_area_size;
    /* stage 1 key offset from data */
    size_t              key_offset;

    /* stage 2 key pad area */
    uint8_t const       *pad_area;
    /* stage 2 key pad total size */
    size_t              pad_area_size;
    /* stage 2 key pad offset within pad_area */
    size_t              pad_offset;
};

/* Return 1 if size if a valid fob file size, 0 otherwise */
int grcf_is_fob_size_valid(size_t size);

/* Create a new fob file of a give size
 */
int grcf_fob_create(char const *path_fob, size_t size);

/* Create an in-memory fob from an existing fob file.
 *
 * On success, the following fields are available:
 *      data
 *      size
 *      pepper_offset
 */
int grcf_fob_map(struct grcf_key_fob *fob, char const *path_fob,
                 char const *passwd, size_t passwd_len);

/* Initialize key and pad information in an in-memory fob previously mapped
 * by grcf_map_fob
 *
 * Calculate the pad region based on the actual fob size
 * Choose key and pad offsets at random within respective regions.
 *
 * Compute and store plaintext fob digest (32 bytes) in dgst.
 */
int grcf_fob_init_new(struct grcf_key_fob *fob, uint8_t *dgst, size_t dgst_size);

/* Initialize key and pad information in an in-memory fob previously mapped
 * by grcf_map_fob based on information in stage 1 header.  Also compute
 * the digest and verify against the supplied value (from the header).
 */
int grcf_fob_init(struct grcf_key_fob *fob,
                  size_t key_area_size, size_t key_offset, size_t pad_offset,
                  uint8_t const *dgst, size_t dgst_size);

/* Clean up and release a mapped fob */
int grcf_fob_unmap(struct grcf_key_fob *fob);

#endif
