/*
 * Copyright (c) 2022 Denis Remezov
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include "crypto_dgst.h"
#include "status_codes.h"

#include <mbedtls/sha256.h>


int grcf_sha256(uint8_t *dgst, size_t dgst_size, uint8_t const *msg, size_t msg_size)
{
    if (dgst_size != GRCF_SHA256_DGST_SIZE) return STATUS_INVALID_DGST_SIZE;
    return mbedtls_sha256(msg, msg_size, dgst, 0) == 0 ? 0 : STATUS_MBEDTLS_SHA256_FAILED;
}
