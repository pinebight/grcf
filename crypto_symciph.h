/*
 * Copyright (c) 2021-2022 Denis Remezov
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#ifndef GRCF_SYM_CIPHERS_H_INCLUDED
#define GRCF_SYM_CIPHERS_H_INCLUDED

#include <stdint.h>
#include <stddef.h>


/* Crypto support for symmetric encryption and decryption.
 *
 * This provides a bridge interface to the actual implementation
 * of crypto operations, which can be selected at build time.
 */

/* Cipher parameters (bytes)
 * Currently fixed.
 */
#define GRCF_SYM_KEY_SIZE       32      /* All symemtric keys */
#define GRCF_AEAD_IV_SIZE       12      /* Nonces used with AEAD ciphers */
#define GRCF_AUTHTAG_SIZE       16      /* Tags used with AEAD ciphers */
#define GRCF_CTR_IV_SIZE        16      /* Nonces used with CTR ciphers */

/* Perform authenticated symmetric encryption given a key and an IV
 *
 * The current scheme is fixed at AES-GCM-256 and the following configuration:
 * - IV/nonce:              12 bytes
 * - Authentication tag:    16 bytes
 *
 * Destination buffer format:
 *      ciphertext:         <source file size>
 *      auth. tag:          16
 *
 * Return 0 on success, STATUS_... on error.
 */
int grcf_encrypt_auth(uint8_t *dst, size_t dst_size,
                      uint8_t const *src, size_t src_size,
                      uint8_t const *key, size_t key_size,
                      uint8_t const *iv, size_t iv_size);

/* Perform symmetric decryption and authentication given a key and an IV
 *
 * This is the inverse of grcf_encrypt_auth().
 *
 * Return 0 on success;
 * STATUS_DEC_AUTH_FAILED on authentication error;
 * STATUS_... on other error conditions.
 */
int grcf_decrypt_auth(uint8_t *dst, size_t dst_size,
                      uint8_t const *src, size_t src_size,
                      uint8_t const *key, size_t key_size,
                      uint8_t const *iv, size_t iv_size);

/* Perform symmetric encryption or decryption with a key stream cipher given
 * a key and an IV
 *
 * Note: since we are using a key stream cipher with no authentication, the
 * same function answers for both directions.
 *
 * The current scheme is fixed at AES-CTR-256 and the following configuration:
 * - IV/nonce:              16 bytes
 *
 * Return 0 on success, STATUS_... on error.
 */
int grcf_crypt_ctr(uint8_t *dst, size_t dst_size,
                   uint8_t const *src, size_t src_size,
                   uint8_t const *key, size_t key_size,
                  uint8_t const *iv, size_t iv_size);

#endif
