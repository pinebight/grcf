/*
 * Copyright (c) 2022 Denis Remezov
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#ifndef GRCF_CONTROL_IO_H_INCLUDED
#define GRCF_CONTROL_IO_H_INCLUDED

#include "common.h"

#include <stddef.h>


/* Return 1 if we require tty access, else 0
 *
 * The determination is based on the command requested and whether stdin
 * is a terminal device.
 */
int need_tty(struct grcf_args const *pa);

/* Read the password from stdin, if necessary
 *
 * If stdin is a terminal then turn off the echo and prompt for
 * interactive password entry; otherwise read directly.
 */
int read_passwd(struct grcf_args *pa);

/* Read IPC object names from stdin, expecting:
 * sem0:/<64-char name>.sem
 * sem1:/<64-char name>.sem
 * shm:/<64-char name>.shm
 */
int read_ipc_names(struct grcf_stdin_params *prm);

/* Print a diagnostic message about status on stderr */
void print_error(struct grcf_args const *pa, int status, int ext_status);

#endif
