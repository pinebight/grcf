#!/bin/sh

TEST_DIR=t_data

ITER_FOB=10
ITER_FILE=20
ITER_CRYPT_FULL=10
ITER_CRYPT_PASS=10
ITER_CRYPT_KEY=10

# 2 MB (adhoc)
DATA_SIZE_MAX=$((0x200000))

KEY_SIZE=32

# 4KB, see key_fob.h
FOB_SIZE_MIN=$((0x1000))
# 16MB (adhoc)
FOB_SIZE_MAX=$((0x1000000))

# Minimum padding page size, see file_crypt.h
PPAGE_SIZE_MIN=1024

# Number of pad paging options
# (0 - auto; [1-N_PPAGE_LEVELS) - shifts for PPAGE_SIZE_MIN)
N_PPAGE_LEVELS=9

path_i=""
path_fob=""
path_key=""

size=0
fob_size=0

rc=0

test_enc_dec_full()
{
    local i=0
    local path_e="$path_i".enc
    local path_d="$path_i".dec
    local passwd="$1"
    local ppage_shift_plus1="$2"

    printf "Performing %d iterations of 3-stage encryption/decryption; file size: %d\n" \
            "$ITER_CRYPT_FULL" "$size"

    while [ "$i" -lt "$ITER_CRYPT_FULL" ]; do
        if [ "$ppage_shift_plus1" -eq 0 ]; then
            # Use automatic padding paging
            echo "$passwd" | ./grcf enc -i "$path_i" -o "$path_e" -c full -k "$path_fob"
        else
            local ppage_shift=$((ppage_shift_plus1 - 1))
            local ppage_size=$((PPAGE_SIZE_MIN << ppage_shift))
            echo "$passwd" | ./grcf enc -i "$path_i" -o "$path_e" -c full -k "$path_fob" -p "$ppage_size"
        fi
        if [ $? -ne 0 ]; then
            printf "Encryption failed\n"
            rc=11
            return
        fi

        echo "$passwd" | ./grcf dec -i "$path_e" -o "$path_d" -c full -k "$path_fob"
        if [ $? -ne 0 ]; then
            printf "Decryption failed\n"
            rc=12
            return
        fi

        cmp "$path_i" "$path_d"
        if [ $? -ne 0 ]; then
            printf "Decrypted file different to original\n"
            rc=13
            return
        fi

        i=$((i + 1))
    done
}

test_enc_dec_pass()
{
    local i=0
    local path_e="$path_i".enc
    local path_d="$path_i".dec
    local passwd="$1"

    printf "Performing %d iterations of password-based encryption/decryption; file size: %d\n" \
           "$ITER_CRYPT_PASS" "$size"

    while [ $i -lt $ITER_CRYPT_PASS ]; do
        echo "$passwd" | ./grcf enc -i "$path_i" -o "$path_e" -c pass
        if [ $? -ne 0 ]; then
            printf "Encryption failed\n"
            rc=21
            return
        fi

        echo "$passwd" | ./grcf dec -i "$path_e" -o "$path_d" -c pass
        if [ $? -ne 0 ]; then
            printf "Decryption failed\n"
            rc=22
            return
        fi

        cmp "$path_i" "$path_d"
        if [ $? -ne 0 ]; then
            printf "Decrypted file different to original\n"
            rc=23
            return
        fi

        i=$((i + 1))
    done
}

test_enc_dec_key()
{
    local i=0
    local path_e="$path_i".enc
    local path_d="$path_i".dec

    printf "Performing %d iterations of encryption/decryption; file size: %d\n" \
           "$ITER_CRYPT_KEY" "$size"

    while [ $i -lt $ITER_CRYPT_KEY ]; do
        ./grcf enc -i "$path_i" -o "$path_e" -c key -k "$path_key"
        if [ $? -ne 0 ]; then
            printf "Encryption failed\n"
            rc=31
            return
        fi

        ./grcf dec -i "$path_e" -o "$path_d" -c key -k "$path_key"
        if [ $? -ne 0 ]; then
            printf "Decryption failed\n"
            rc=32
            return
        fi

        cmp "$path_i" "$path_d"
        if [ $? -ne 0 ]; then
            printf "Decrypted file different to original\n"
            rc=33
            return
        fi

        i=$((i + 1))
    done
}

gen_data_file()
{
    path_i=$(mktemp "$TEST_DIR"/tmp.data.XXXXXX)
    rc=$?
    [ $rc -eq 0 ] || return

    # remove leading space and 0 and trailing space for the sake of some picky shells
    size=$(od -A n -t d -N 3 /dev/urandom | sed 's/^[ 0]*//;s/ *$//')
    size=$((size + 1))
    size=$((size % DATA_SIZE_MAX))

    dd if=/dev/urandom of="$path_i" bs=1 count="$size"
    rc=$?
}

gen_key_file()
{
    path_key=$(mktemp "$TEST_DIR"/tmp.key.XXXXXX)
    rc=$?
    [ "$rc" -eq 0 ] || return

    dd if=/dev/urandom of="$path_key" bs="$KEY_SIZE" count=1
    rc=$?
}

gen_fob_file()
{
    path_fob=$(mktemp "$TEST_DIR"/tmp.fob.XXXXXX)
    rc=$?
    [ "$rc" -eq 0 ] || return

    fob_size=$(od -A n -t d -N 3 /dev/urandom | sed 's/^[ 0]*//;s/ *$//')
    fob_size=$((fob_size + FOB_SIZE_MIN))
    fob_size=$((fob_size % FOB_SIZE_MAX))

    printf "Fob size: %d\n" $fob_size
    ./grcf fob -o "$path_fob" -s "$fob_size"
    rc=$?
}

rm_data_files()
{
    local path="$path_i"
    [ -n "$path" ] && [ -f "$path" ] && rm "$path"
    path="$path_i".enc
    [ -n "$path" ] && [ -f "$path" ] && rm "$path"
    path="$path_i".dec
    [ -n "$path" ] && [ -f "$path" ] && rm "$path"
    path_i=""
}

rm_key_file()
{
    [ -n "$path_key" ] && [ -f "$path_key" ] && rm "$path_key"
    path_key=""
}

rm_fob_file()
{
    [ -n "$path_fob" ] && [ -f "$path_fob" ] && rm "$path_fob"
    path_fob=""
}

iter_file_3stage()
{
    local page_shift_p1=0
    local i=0
    while [ "$i" -lt "$ITER_FILE" ]; do
        gen_data_file
        [ "$rc" -eq 0 ] || return

        page_shift_p1=$(od -A n -t d -N 2 /dev/urandom | sed 's/^[ 0]*//;s/ *$//')
        page_shift_p1=$((page_shift_p1 % N_PPAGE_LEVELS))

        test_enc_dec_full 1234 "$page_shift_p1"
        [ "$rc" -eq 0 ] || return

        rm_data_files

        i=$((i + 1))
    done
}

iter_file_1stage()
{
    local i=0
    while [ "$i" -lt "$ITER_FILE" ]; do
        gen_data_file
        [ "$rc" -eq 0 ] || return

        test_enc_dec_pass q33wert67yJKDDoirpsfd
        [ "$rc" -eq 0 ] || return

        test_enc_dec_key
        [ "$rc" -eq 0 ] || return

        rm_data_files

        i=$((i + 1))
    done
}

do_main()
{
    mkdir -p "$TEST_DIR"

    gen_key_file
    [ "$rc" -eq 0 ] || return

    iter_file_1stage
    [ "$rc" -eq 0 ] || return

    rm_key_file

    local i=0
    while [ "$i" -lt "$ITER_FOB" ]; do
        gen_fob_file
        [ "$rc" -eq 0 ] || return

        iter_file_3stage
        [ "$rc" -eq 0 ] || return

        rm_fob_file

        i=$((i + 1))
    done
}

do_main

# Clean up
rm_data_files
rm_key_file
rm_fob_file

exit "$rc"
