/*
 * Copyright (c) 2021-2022 Denis Remezov
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include "crypto_symciph.h"
#include "status_codes.h"

#include <mbedtls/aes.h>
#include <mbedtls/cipher.h>
#include <mbedtls/gcm.h>
#include <string.h>


int grcf_encrypt_auth(uint8_t *dst, size_t dst_size,
                      uint8_t const *src, size_t src_size,
                      uint8_t const *key, size_t key_size,
                      uint8_t const *iv, size_t iv_size)
{
    int                     ret = 0;
    mbedtls_gcm_context     ctx;

    if (iv_size != GRCF_AEAD_IV_SIZE) return STATUS_INVALID_IV_SIZE;
    if (key_size != GRCF_SYM_KEY_SIZE) return STATUS_INVALID_KEY_SIZE;
    if (dst_size != src_size + GRCF_AUTHTAG_SIZE || dst_size < src_size) {
        return STATUS_CE_INVALID_BUFFER_SIZE;
    }

    memset(&ctx, 0, sizeof(ctx));
    mbedtls_gcm_init(&ctx);

    ret = mbedtls_gcm_setkey(&ctx, MBEDTLS_CIPHER_ID_AES, key, key_size << 3);
    if (ret != 0) {
        ret = STATUS_MBEDTLS_GCM_SETKEY_FAILED;
        goto out;
    }

    ret = mbedtls_gcm_crypt_and_tag(&ctx,
                                    MBEDTLS_GCM_ENCRYPT,
                                    src_size,
                                    iv, iv_size,
                                    NULL, 0,    /* no AAD */
                                    src,
                                    dst,
                                    GRCF_AUTHTAG_SIZE,
                                    dst + src_size);
    if (ret != 0) {
        ret = (ret == MBEDTLS_ERR_GCM_BAD_INPUT) ?
            STATUS_MBEDTLS_GCM_ENC_BAD_INPUT : STATUS_MBEDTLS_GCM_ENC_CIPHER_ERROR;
    }

out:
    mbedtls_gcm_free(&ctx);

    return ret;
}

int grcf_decrypt_auth(uint8_t *dst, size_t dst_size,
                      uint8_t const *src, size_t src_size,
                      uint8_t const *key, size_t key_size,
                      uint8_t const *iv, size_t iv_size)
{
    int                     ret = 0;
    mbedtls_gcm_context     ctx;

    if (iv_size != GRCF_AEAD_IV_SIZE) return STATUS_INVALID_IV_SIZE;
    if (key_size != GRCF_SYM_KEY_SIZE) return STATUS_INVALID_KEY_SIZE;
    if (dst_size + GRCF_AUTHTAG_SIZE != src_size || src_size < dst_size) {
        return STATUS_CE_INVALID_BUFFER_SIZE;
    }

    memset(&ctx, 0, sizeof(ctx));
    mbedtls_gcm_init(&ctx);

    ret = mbedtls_gcm_setkey(&ctx, MBEDTLS_CIPHER_ID_AES, key, key_size << 3);
    if (ret != 0) {
        ret = STATUS_MBEDTLS_GCM_SETKEY_FAILED;
        goto out;
    }

    ret = mbedtls_gcm_auth_decrypt(&ctx,
                                   dst_size,
                                   iv, iv_size,
                                   NULL, 0,     /* no AAD */
                                   src + dst_size,
                                   GRCF_AUTHTAG_SIZE,
                                   src,
                                   dst);
    if (ret != 0) {
        if (ret == MBEDTLS_ERR_GCM_AUTH_FAILED) {
            ret = STATUS_DEC_AUTH_FAILED;
        } else if (ret == MBEDTLS_ERR_GCM_BAD_INPUT) {
            ret = STATUS_MBEDTLS_GCM_DEC_BAD_INPUT;
        } else {
            ret = STATUS_MBEDTLS_GCM_DEC_CIPHER_ERROR;
        }
    }

out:
    mbedtls_gcm_free(&ctx);

    return ret;
}

int grcf_crypt_ctr(uint8_t *dst, size_t dst_size,
                   uint8_t const *src, size_t src_size,
                   uint8_t const *key, size_t key_size,
                   uint8_t const *iv, size_t iv_size)
{
    int                     ret = 0;
    mbedtls_aes_context     ctx;
    size_t                  nc_off = 0;
    uint8_t                 nonce_counter[16];
    uint8_t                 stream_block[16];

    if (iv_size != GRCF_CTR_IV_SIZE) return STATUS_INVALID_IV_SIZE;
    if (key_size != GRCF_SYM_KEY_SIZE) return STATUS_INVALID_KEY_SIZE;
    if (dst_size != src_size ) return STATUS_CE_INVALID_BUFFER_SIZE;

    memset(&ctx, 0, sizeof(ctx));

    mbedtls_aes_init(&ctx);

    ret = mbedtls_aes_setkey_enc(&ctx, key, key_size << 3);
    if (ret != 0) {
        ret = STATUS_MBEDTLS_AES_SETKEY_FAILED;
        goto out;
    }

    memcpy(nonce_counter, iv, sizeof(nonce_counter));
    memset(stream_block, 0, sizeof(stream_block));
    ret = mbedtls_aes_crypt_ctr(&ctx,
                                src_size,
                                &nc_off,
                                nonce_counter,
                                stream_block,
                                src,
                                dst);
    if (ret != 0) {
        ret = STATUS_MBEDTLS_AES_CRYPT_CTR_FAILED;
    }

out:
    mbedtls_aes_free(&ctx);

    return ret;
}
