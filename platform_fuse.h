/*
 * Copyright (c) 2022 Denis Remezov
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#ifndef GRCF_PLATFORM_FUSE_H_INCLUDED
#define GRCF_PLATFORM_FUSE_H_INCLUDED

#define FUSE_USE_VERSION    29

#if defined(__APPLE__)

/* Darwin */
#include <osxfuse/fuse/fuse.h>

#define st_atim st_atimespec
#define st_mtim st_mtimespec
#define st_ctim st_ctimespec

#elif defined(__OpenBSD__)

/* OpenBSD */
#include <fuse.h>

#else

/* Default (including  Linux, FreeBSD) */
#include <fuse/fuse.h>

#endif

#endif
