# GR Crypto Filter (GRCF)
# (C) Denis Remezov 2021-2022

# Executable name
PROGRAM =       grcf

# Helper script
PROGRAM_UTIL =  grcfctl

# Installation directory prefix
PREFIX ?=       /usr/local
BINDIR =        ${PREFIX}/bin

UNAME !=        uname

# Redefine to select an alternative
# Options:
#   openssl
#   mbedtls
#
CRYPTOLIB ?=    openssl

FLAGS_COMMON =  -pthread
CFLAGS_FUSE =   -D_FILE_OFFSET_BITS=64

CFLAGS =        -Wall -Werror ${CFLAGS_FUSE} ${FLAGS_COMMON} -I/usr/local/include

# USE_MLOCK: Enable memory locking for sensitive data
# The success of such locking will depend on the configured resource limits
# (On Linux, see e.g. ulimit(1) and limits.conf(5)).
# This is at best a partial solution, as some sensitive data would still be
# present both in grcf program memory and likely elsewhere in the system (e.g.
# password entry).  A more complete solution is to disable or encrypt the swap.
# USE_MLOCK = 1

.ifdef USE_MLOCK
    CFLAGS +=   -DGRCF_USE_MLOCK
.endif

.if "${UNAME}" == "OpenBSD"
# Can't pledge mlock as of now, and we also can't easily mlock in advance, so
# pledge and mlock are mutually exclusive (pledge being the default).
.ifndef USE_MLOCK
    CFLAGS +=   -DGRCF_USE_PLEDGE
.endif
.endif

LDFLAGS =       ${FLAGS_COMMON}
LDLIBS =

HEADERS !=      ls *.h

SRCS = \
                file_crypt.c \
                control_io.c \
                key_fob.c \
                grcf.c \
                platform_default_util.c \
                serve_mount.c \
                serve_shm.c \
                util.c

.if "${CRYPTOLIB}" == "openssl"
    SRCS += \
                openssl_dgst.c \
                openssl_pbkdf.c \
                openssl_symciph.c

    LDLIBS +=   -lcrypto
.elif "${CRYPTOLIB}" == "mbedtls"
    SRCS += \
                mbedtls_dgst.c \
                mbedtls_pbkdf.c \
                mbedtls_symciph.c

    LDLIBS +=   -lmbedcrypto
.endif

LDLIBS +=       -lfuse

OBJS =          ${SRCS:S/.c$/.o/g}

all:            ${PROGRAM}

install:	    ${PROGRAM}
	install -s ${PROGRAM} ${BINDIR}
	install ${PROGRAM_UTIL} ${BINDIR}

uninstall:
	rm -f ${BINDIR}/${PROGRAM_UTIL}
	rm -f ${BINDIR}/${PROGRAM}

${PROGRAM}:     ${OBJS}
	${CC} -o $@ ${OBJS} ${LDLIBS} ${LDFLAGS}

${OBJS}:        ${SRCS} ${HEADERS}

clean:
	rm -f *.o
	rm -f ${PROGRAM}

.PHONY: clean uninstall
