/*
 * Copyright (c) 2022 Denis Remezov
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#ifndef GRCF_CRYPTO_DGST_H_INCLUDED
#define GRCF_CRYPTO_DGST_H_INCLUDED

#include <stddef.h>
#include <stdint.h>


/* Crypto support for hash functions
 *
 * This provides a bridge interface to the actual implementation
 * of crypto operations, which can be selected at build time.
 */

/* SHA-256 digest size, bytes */
#define GRCF_SHA256_DGST_SIZE           32

/* Compute a SHA-2 256-bit digest
 *
 * return 0 on success, STATUS_... on error.
 */
int grcf_sha256(uint8_t *dgst, size_t dgst_size, uint8_t const *msg, size_t msg_size);

#endif
