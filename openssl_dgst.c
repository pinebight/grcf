/*
 * Copyright (c) 2022 Denis Remezov
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include "crypto_dgst.h"
#include "status_codes.h"

#include <openssl/evp.h>


int grcf_sha256(uint8_t *dgst, size_t dgst_size, uint8_t const *msg, size_t msg_size)
{
    int             ret = 0;
    EVP_MD const    *dgst_type = EVP_sha256();
    EVP_MD_CTX      *ctx = NULL;
    unsigned        ossl_dgst_len = GRCF_SHA256_DGST_SIZE;

    if (dgst_size != GRCF_SHA256_DGST_SIZE || dgst_size != EVP_MD_size(dgst_type)) {
        return STATUS_INVALID_DGST_SIZE;
    }

    ctx = EVP_MD_CTX_new();
    if (ctx == NULL) return STATUS_MD_CTX_FAILED;

    if (EVP_DigestInit_ex(ctx, dgst_type, NULL) != 1) {
        ret = STATUS_DIGEST_INIT_FAILED;
        goto out;
    }
    if (EVP_DigestUpdate(ctx, msg, msg_size) != 1) {
        ret = STATUS_DIGEST_UPDATE_FAILED;
        goto out;
    }
    if (EVP_DigestFinal_ex(ctx, dgst, &ossl_dgst_len) != 1) {
        ret = STATUS_DIGEST_FINAL_FAILED;
        goto out;
    }
    if (ossl_dgst_len != GRCF_SHA256_DGST_SIZE) {
        ret = STATUS_DIGEST_BAD_FINAL_SIZE;
    }

out:
    EVP_MD_CTX_free(ctx);

    return ret;
}
