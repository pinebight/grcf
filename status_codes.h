/*
 * Copyright (c) 2021-2022 Denis Remezov
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#ifndef GRCF_STATUS_CODES_H_INCLUDED
#define GRCF_STATUS_CODES_H_INCLUDED

/* Enum values are assumed at least 32-bit-wide.
 * The grcf program uses at most the lower 24 bits [23, 0], leaving bits
 * [31, 24] for front end's use.  This allows front ends to share their error
 * spaces with grcf proper for simpler error status tracking.
 */

/* Flags indicating the presence of extended error information */
enum {
    /* errno is set */
    STATUSF_ERRNO                               = 0x10000,
    /* External error code is available and set */
    STATUSF_EXT_STATUS                          = 0x20000
};

/* Do not delete, re-arrange or change existing values.
 * If they become disused, existing enums may be renamed to mark them
 * deprecated.
 * Add new values at the end of a group.
 */
enum {
    STATUS_SUCCESS                              = 0,

    /* General and program-level validation */
    STATUS_OOM                                  = 0x0001,
    STATUS_NOT_IMPLEMENTED                      = 0x0002,
    STATUS_NO_COMMAND                           = 0x0003,
    STATUS_INVALID_ARG                          = 0x0004,
    STATUS_INVALID_COMMAND                      = 0x0005,
    STATUS_INVALID_IV_SIZE                      = 0x0006,
    STATUS_INVALID_KEY_SIZE                     = 0x0007,
    STATUS_SIZE_OUT_OF_RANGE                    = 0x0008,
    STATUS_DECRYPT_BAD_SRC_SIZE                 = 0x0009,
    STATUS_ZERO_LEN_FILE_UNUSED                 = 0x000a,   /* no longer used */
    STATUS_INVALID_KEY_FILE_SIZE                = 0x000b,
    STATUS_INVALID_KEY_FOB_FILE_SIZE            = 0x000c,
    STATUS_INVALID_PASSWORD_SIZE                = 0x000d,
    STATUS_INVALID_SALT_SIZE                    = 0x000e,
    STATUS_PASSWORD_INPUT_ERROR                 = 0x000f,
    STATUS_PASSWORD_EMPTY                       = 0x0010,
    STATUS_PASSWORD_TOO_LONG                    = 0x0011,
    STATUS_PASSWORD_REENTRY_MISMATCH            = 0x0012,
    STATUS_PASSWORD_DISALLOWED_CHAR             = 0x0013,
    STATUS_INVALID_MODE                         = 0x0014,
    STATUS_DATA_SIZE_OVERFLOW                   = 0x0015,
    STATUS_INVALID_PPAGE_SIZE                   = 0x0016,
    STATUS_INVALID_FOB_DGST_SIZE                = 0x0017,
    STATUS_FOB_DGST_MISMATCH                    = 0x0018,
    STATUS_INVALID_DGST_SIZE                    = 0x0019,
    STATUS_BAD_STDIN                            = 0x001a,
    STATUS_PATH_INVALID                         = 0x001b,
    STATUS_PATH_TOO_LONG                        = 0x001c,
    STATUS_DEC_AUTH_FAILED                      = 0x001d,
    STATUS_NO_PASSWD_NEEDED                     = 0x001e,
    STATUS_IPC_PARAM_INPUT_ERROR                = 0x001f,
    STATUS_IPC_PARAM_BAD_VALUE                  = 0x0020,
    STATUS_IPC_PARAM_DUP_VALUE                  = 0x0021,
    STATUS_IPC_BAD_SHM_SIZE                     = 0x0022,
    STATUS_IPC_BAD_SHM_MAPPED_SIZE              = 0x0023,
    STATUS_IPC_BAD_SHM_PAYLOAD_SIZE             = 0x0024,

    /* User input (command, options) validation */
    STATUS_VALIDATION_NO_CMD                    = 0x1001,
    STATUS_VALIDATION_BAD_CMD                   = 0x1002,
    STATUS_VALIDATION_NO_INPUT_PATH             = 0x1003,
    STATUS_VALIDATION_NO_OUTPUT_PATH            = 0x1004,
    STATUS_VALIDATION_SPURIOUS_FOB_SIZE         = 0x1005,
    STATUS_VALIDATION_NO_MODE                   = 0x1006,
    STATUS_VALIDATION_NO_KEY_PATH               = 0x1007,
    STATUS_VALIDATION_SPURIOUS_KEY_PATH         = 0x1008,
    STATUS_VALIDATION_SPURIOUS_INPUT_PATH       = 0x1009,
    STATUS_VALIDATION_SPURIOUS_MODE             = 0x100a,
    STATUS_VALIDATION_SPURIOUS_PPAGE_SIZE       = 0x100b,
    STATUS_VALIDATION_NO_FOB_PATH               = 0x100c,
    STATUS_VALIDATION_INVALID_MODE              = 0x100d,
    STATUS_VALIDATION_INVALID_PDSIZE            = 0x100e,
    STATUS_VALIDATION_PDSIZE_LOW                = 0x100f,
    STATUS_VALIDATION_PDSIZE_HIGH               = 0x1010,
    STATUS_VALIDATION_PDSZIE_NOT_POW2           = 0x1011,
    STATUS_VALIDATION_NO_FOB_OUT_PATH           = 0x1012,
    STATUS_VALIDATION_NO_FOB_SIZE               = 0x1013,
    STATUS_VALIDATION_BAD_FOB_SIZE              = 0x1014,
    STATUS_VALIDATION_IN_OUT_PATHS_EQUAL        = 0x1015,
    STATUS_VALIDATION_IN_KEY_PATHS_EQUAL        = 0x1016,
    STATUS_VALIDATION_OUT_KEY_PATHS_EQUAL       = 0x1017,
    STATUS_VALIDATION_OUT_PATH_NOT_A_FILE       = 0x1018,
    STATUS_VALIDATION_NO_MOUNT_PATH             = 0x1019,
    STATUS_VALIDATION_SPURIOUS_MOUNT_PATH       = 0x101a,
    STATUS_VALIDATION_MOUNT_PATH_NOT_A_DIR      = 0x101b,
    STATUS_VALIDATION_SPURIOUS_MOUNT_RO         = 0x101c,
    STATUS_VALIDATION_NO_IPC_NAME               = 0x101d,
    STATUS_VALIDATION_DUPLICATE_OPT             = 0x101e,
    STATUS_VALIDATION_SPURIOUS_OUTPUT_PATH      = 0x101f,

    /* Unexpected conditions that may be indicative of partial crypto breakage,
       programming error, or using an old program version for decryption */
    STATUS_DECRYPT_BAD_PAYLOAD_SIZE             = 0x2001,
    STATUS_DECRYPT_INVALID_VERSION              = 0x2002,
    STATUS_DECRYPT_INVALID_PROFILE              = 0x2003,
    STATUS_BAD_KEY_OFFSET                       = 0x2004,
    STATUS_BAD_PAD_OFFSET                       = 0x2005,
    STATUS_BAD_KEY_AREA_SIZE                    = 0x2006,

    /* Logic error validation (truly unexpected) */
    STATUS_CE_INVALID_BUFFER_SIZE               = 0x3001,
    STATUS_CE_INVALID_KEY_SIZE                  = 0x3002,
    STATUS_CE_INVALID_PEPPER_SIZE               = 0x3003,
    STATUS_CE_DEC3ST_BOTH_FILE_MAPPING_GIVEN    = 0x3004,
    STATUS_CE_DEC3ST_BOTH_FILE_MAPPING_MISSING  = 0x3005,
    STATUS_CE_FORMAT_IPC_NAME_FAILED            = 0x3006,

    /* OpenSSL */
    STATUS_CIPHER_CTX_FAILED                    = 0x4001,
    STATUS_AES_256_GCM_FAILED                   = 0x4002,
    STATUS_ENC_INIT_FAILED                      = 0x4003,
    STATUS_DEC_INIT_FAILED                      = 0x4004,
    STATUS_SET_IVLEN_FAILED                     = 0x4005,
    STATUS_ENC_UPDATE_FAILED                    = 0x4006,
    STATUS_DEC_UPDATE_FAILED                    = 0x4007,
    STATUS_ENC_FINAL_FAILED                     = 0x4008,
    STATUS_DEC_FINAL_FAILED_UNUSED              = 0x4009,   /* replaced by STATUS_DEC_AUTH_FAILED */
    STATUS_ENC_GETTAG_FAILED                    = 0x400a,
    STATUS_DEC_SETTAG_FAILED                    = 0x400b,
    STATUS_ENC_BAD_UPDATE_SIZE                  = 0x400c,
    STATUS_ENC_BAD_FINAL_SIZE                   = 0x400d,
    STATUS_DEC_BAD_UPDATE_SIZE                  = 0x400e,
    STATUS_RAND_BYTES_FAILED                    = 0x400f,
    STATUS_PBKDF2_HMAC_FAILED                   = 0x4010,
    STATUS_AES_256_CTR_FAILED                   = 0x4011,
    STATUS_MD_CTX_FAILED                        = 0x4012,
    STATUS_DIGEST_INIT_FAILED                   = 0x4013,
    STATUS_DIGEST_UPDATE_FAILED                 = 0x4014,
    STATUS_DIGEST_FINAL_FAILED                  = 0x4015,
    STATUS_DIGEST_BAD_FINAL_SIZE                = 0x4016,

    /* mbedTLS */
    STATUS_MBEDTLS_AES_SETKEY_FAILED            = 0x4101,
    STATUS_MBEDTLS_AES_CRYPT_CTR_FAILED         = 0x4102,
    STATUS_MBEDTLS_GCM_SETKEY_FAILED            = 0x4103,
    STATUS_MBEDTLS_GCM_ENC_BAD_INPUT            = 0x4104,
    STATUS_MBEDTLS_GCM_ENC_CIPHER_ERROR         = 0x4105,
    STATUS_MBEDTLS_GCM_DEC_BAD_INPUT            = 0x4106,
    STATUS_MBEDTLS_GCM_DEC_CIPHER_ERROR         = 0x4107,
    STATUS_MBEDTLS_SHA256_FAILED                = 0x4108,
    STATUS_MBEDTLS_MD_INFO_NOT_FOUND            = 0x4109,
    STATUS_MBEDTLS_MD_SETUP_FAILED              = 0x410a,
    STATUS_MBEDTLS_PBKDF2_HMAC_FAILED           = 0x410b,

    /* I/O and system failures */
    STATUS_OPEN_RD_FAILED                       = 0x5018 | STATUSF_ERRNO,
    STATUS_OPEN_WR_FAILED                       = 0x5019 | STATUSF_ERRNO,
    STATUS_STAT_FAILED                          = 0x501a | STATUSF_ERRNO,
    STATUS_MMAP_RD_FAILED                       = 0x501b | STATUSF_ERRNO,
    STATUS_MMAP_WR_FAILED                       = 0x501c | STATUSF_ERRNO,
    STATUS_MLOCK_RD_FAILED                      = 0x501d | STATUSF_ERRNO,
    STATUS_MLOCK_WR_FAILED                      = 0x501e | STATUSF_ERRNO,
    STATUS_FTRUNCATE_FAILED                     = 0x501f | STATUSF_ERRNO,
    STATUS_TCGETATTR_FAILED                     = 0x5020 | STATUSF_ERRNO,
    STATUS_TCSETATTR_FAILED                     = 0x5021 | STATUSF_ERRNO,
    STATUS_UNALIGNED_MAPPING                    = 0x5022,
    STATUS_REALPATH_FAILED                      = 0x5023 | STATUSF_ERRNO,
    STATUS_GETCWD_FAILED                        = 0x5024 | STATUSF_ERRNO,
    STATUS_FUSE_MAIN_FAILED                     = 0x5025 | STATUSF_EXT_STATUS,
    STATUS_FUSE_OP_INIT_FAILED                  = 0x5026,
    STATUS_FAILED_TO_GET_PAGE_SIZE              = 0x5027,
    STATUS_UNEXPECTED_FUSE_EXIT                 = 0x5028,
    STATUS_MKSTEMP_FAILED                       = 0x5029 | STATUSF_ERRNO,
    STATUS_RENAME_FAILED                        = 0x502a | STATUSF_ERRNO,
    STATUS_DEV_RANDOM_OPEN_FAILED               = 0x502b | STATUSF_ERRNO,
    STATUS_DEV_RANDOM_READ_FAILED               = 0x502c | STATUSF_ERRNO,
    STATUS_SEM_OPEN_FAILED                      = 0x502d | STATUSF_ERRNO,
    STATUS_SEM_POST_FAILED                      = 0x502e | STATUSF_ERRNO,
    STATUS_SHM_OPEN_FAILED                      = 0x502f | STATUSF_ERRNO,
    STATUS_PLEDGE_FAILED                        = 0x5030 | STATUSF_ERRNO
};

#define STATUS_HAS_ERRNO(s)     (((s) & STATUSF_ERRNO) != 0)
#define STATUS_HAS_EXT(s)       (((s) & STATUSF_EXT_STATUS) != 0)

#endif
