# Common defintions for test_basic_enc.sh and test_basic_dec.sh

TEST_DIR=t_data

FOB_SIZE=40101
KEY_SIZE=32

TEST_PASSWD="1234"

path_i=""
path_fob=""
path_key=""

size=0
rc=0

# File sizes to test
FILE_SIZES="0 1 13 1024 32000 5123456"
